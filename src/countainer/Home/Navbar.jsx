import React , { Component, Fragment}from 'react';
import './Navbar.css';
import Logout from '../../component/logout/logout';


class Navbar extends Component{

    state = {
        user: []
    }

    componentDidMount(){
       let datas = JSON.parse(localStorage.getItem('user'));
              
       
       this.setState({
        user : datas
       })
  
    }

    render(){
        return(
            <Fragment>
     <div>
   
    <div id="wrapper">
        
        <nav className="navbar navbar-default navbar-static-top m-b-0">
      
            <div className="navbar-header">
                <div className="top-left-part">
                   
                    <a className="logo" href="/">
                    <b>
                     <img src="plugins/images/admin-logo.png" alt="home" className="dark-logo" /><img src="../plugins/images/logo.png" height="40" alt="home" className="light-logo" />
                     </b>
                      <span className="hidden-xs">
                       <img src="plugins/images/admin-text.png" alt="home" className="dark-logo" /><img src="../plugins/images/sampun.png" alt="home" height="50" className="light-logo" />
                     </span> </a>
                </div>
               
                <ul className="nav navbar-top-links navbar-left">
                    <li><a href="javascript:void(0)" className="open-close waves-effect waves-light"><i className="ti-menu"></i></a></li>
                    <li className="dropdown">
                       
                       
                    </li>
                    
                
                </ul>
                <ul className="nav navbar-top-links navbar-right pull-right">
                   
                    <li className="dropdown">
                        <a className="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="plugins/images/users/1.jpg" alt="user-img" width="36" className="img-circle" /><b className="hidden-xs">{this.state.user.nama}</b><span className="caret"></span> </a>
                        <ul className="dropdown-menu dropdown-user animated flipInY">
                            <li>
                                <div className="dw-user-box">
                                    <div className="u-img"><img src="../plugins/images/users/1.jpg" alt="user" /></div>
                                    <div className="u-text">
                                        <h4>{this.state.user.nama}</h4></div>
                                </div>
                            </li>
                            <li role="separator" className="divider"></li>
                            <li><a href="#"><i className="ti-user"></i> My Profile</a></li>
                            <li><a href="#"><i className="ti-wallet"></i> My Balance</a></li>
                            <li role="separator" className="divider"></li>
                            <li><a href="#"><i className="ti-settings"></i> Account Setting</a></li>
                            <li role="separator" className="divider"></li>
                            <Logout />
                           
                        </ul>
                    </li>
                </ul>
            </div>
            
        </nav>
        </div>
        </div>
             </Fragment> 
        )
    }
}


export default Navbar;