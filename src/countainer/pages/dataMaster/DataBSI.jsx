import React, {Component, Fragment} from 'react';
import swal from 'sweetalert';
import './DataMaster.css';
import {APIBSI} from '../../../service';



let Tes = (props) => {
   let data = {
        id: props.id,
        kode_bsi : props.kode,
        alamat : props.alamat,
        nama: props.nama,
        nama_director : props.nama_director,
        no_hp : props.no_hp,
        email : props.email
   }
   if (props.status == 1) {

        return (
             <tr>
               
                <td>{props.no}</td>
                <td>{props.kode}</td>
                <td>{props.nama}</td>
                <td>{props.alamat}</td>
                <td>{props.total}</td>
                <td>{props.nama_director}</td>
                <td>{props.no_hp}</td>
                <td>{props.email}</td>
                 
                <td>
                    <button type="button" className="btn-sm btn-info" data-toggle="modal" data-target="#add-update" 
                     onClick={() => props.update(data)}> <i className="mdi mdi-settings"> </i> </button>  </td>
                  <td>
                    <button type="button" className="btn-sm  btn-warning" onClick={() => props.romove(props.id)} > <i className="mdi mdi-delete"></i> </button> </td>
            </tr>
        )
   }else{
        return (
             <tr>
              
              <td>{props.no}</td>
                <td>{props.kode}</td>
                <td>{props.nama}</td>
                <td>{props.alamat}</td>
                <td>{props.total}</td>
                <td>{props.nama_director}</td>
                <td>{props.no_hp}</td>
                <td>{props.email}</td>
                 
                <td>
                    <button type="button" className="btn-sm btn-success"  onClick={() => props.restore(props.id)} > <i className="mdi mdi-update"> </i></button> </td>
                    <td>
                    <button type="button" className="btn-sm  btn-danger" onClick={() => props.deletee(props.id)} > <i className="mdi mdi-delete-forever"></i></button> </td>
            </tr>
        )
   }
   
}


class DataBSI extends Component{

     state = {
        bsi : [],
         postBsi: {
            id: '',
            alamat: '',
            kode_bsi : '',
            nama : '',
            nama_director : '',
            no_hp : '',
            email : ''
        },
        
        isUpdate : false
    }

    getBSI = () => {
        APIBSI.getBSI().then(res => {
           this.setState({
               bsi : res
           })
       })
    }



    postApi = () => {
        const bsi = {
          nama: this.state.postBsi.nama,
          kode_bsi: this.state.postBsi.kode_bsi,
          alamat: this.state.postBsi.alamat,
          nama_director : this.state.postBsi.nama_director,
          no_hp : this.state.postBsi.no_hp,
          email : this.state.postBsi.email
        };

        APIBSI.postBSI(bsi).then((res) => {
            this.getBSI()
        })
        
     }

      updateApi = () => {
        const bsi = {
            nama: this.state.postBsi.nama,
            kode_bsi: this.state.postBsi.kode_bsi,
            alamat: this.state.postBsi.alamat,
            nama_director : this.state.postBsi.nama_director,
            no_hp : this.state.postBsi.no_hp,
            email : this.state.postBsi.email,
            id_data : this.state.postBsi.id
        };
        if (bsi.alamat == '' || bsi.email == '' || bsi.kode_bsi == '' || bsi.nama == '' 
            || bsi.nama_director == '' || bsi.no_hp == '' ) {
                swal({
                    title: "Ops..!",
                    text: "Pastikan Anda Telah Mengisi Semua Data!!!",
                    icon: "error",
                  });
        }else{
            APIBSI.putBSI(bsi).then((res) => {
                this.getBSI()
                this.tambah()
            })
        }
       
     }

    componentDidMount(){
        this.getBSI()
    }

    hendleSubmit = () => {
      
        const  bsi = this.state.postBsi;
        // console.log(bsi.alamat.trim() + bsi.email.trim() + bsi.kode_bsi.trim() + bsi.nama.trim() == '' 
        // && bsi.nama_director.trim() == '' && bsi.no_hp.trim() == '' );
        
        if(bsi.alamat.trim() == '' || bsi.email.trim() == '' || bsi.kode_bsi.trim() == '' || bsi.nama.trim() == '' 
            || bsi.nama_director.trim() == '' || bsi.no_hp.trim() == '' ){
                swal({
                    title: "Ops..!",
                    text: "Pastikan Anda Telah Mengisi Semua Data!!!",
                    icon: "error",
                  });
        }else{
            this.postApi()
            // this.getBSI()
        }
        
    }

    hendeleChange = (event) => {
        let postBsiNew = {...this.state.postBsi};

    
        postBsiNew[event.target.name] = event.target.value

        this.setState({
            postBsi: postBsiNew
        }, () => {
            console.log(this.state.postBsi)
        })
    }

    hendleRemove = (data) => {
        APIBSI.deleteBSI(data).then((res) => {
            this.getBSI()
        })
       
    }


    hendeleRestore = (data) => {
        APIBSI.restoreBSI(data).then((res) => {
            this.getBSI()
        })
         
    }

    hendleDeletePer = (data) => {
        APIBSI.permanenBSI(data).then((res) => {
            this.getBSI()
        })
    }

    hendeleUpdate = (data) => {
    // console.log(data)
        this.setState({
            postBsi: data,
            isUpdate : true
        }, () => {

        })
    }

    updateSubmit = () => {
        this.updateApi()
        this.getBSI()
    }

    tambah = ()=> {
        this.setState({
            postBsi: {
                id: '',
                alamat: '',
                kode_bsi : '',
                nama : '',
                nama_director : '',
                no_hp : '',
                email : ''
            },
            isUpdate : false
        }, () => {
        })
    }

	render(){
        
        let i = 0;
        let j = 0;
		return (
            <Fragment>
            
            <div id="page-wrapper">
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h2 className="page-title" id ="tulis">DATA BSI</h2> 
                    </div>
                </div>
                </div>

                <div className="container">
                <div className="row">
                    <div className="col-sm-11">
                    <div className="white-box">
                            <div className="card-body">
                                <h4 className="card-title">Data BSI</h4>
                                
                                <div className="panel-group" id="accordion">
                                    <div className="panel panel-default">
                                        <div className="panel-heading">
                                        <div className="row">
										    <div className="col-md-9">
										    	 <button type="button" className="btn btn-primary btn-rounded m-t-10 float-right tambahData" data-toggle="modal" data-target="#add-contact"  onClick={this.tambah}>Tambah Data</button>


								<div id="add-contact" className="modal fade in" role="dialog" aria-labelledby="mymodallabel" aria-hidden="true" >
                                    <div className="modal-dialog">
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 className="modal-title" id="mymodallabel"> {this.state.isUpdate ? 'Update' : 'Tambah'}  Data</h4> </div>
                                            <div className="modal-body">
                                                <form className="form-horizontal form-material" onSubmit={this.hendleSubmit} >
                                                    <div className="form-group">
                                                        <input type="hidden" value={this.state.postBsi.id} name="id_data" />
                                                       <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control"  onChange={this.hendeleChange} value={this.state.postBsi.kode_bsi}  name="kode_bsi" placeholder="kode" /> </div>
                                                        
                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control"  onChange={this.hendeleChange}  name="nama" value={this.state.postBsi.nama} placeholder="nama" /> </div>
                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control"  onChange={this.hendeleChange}  name="alamat" value={this.state.postBsi.alamat} placeholder="alamat" /> </div>                                                        
                                                            <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control"  onChange={this.hendeleChange}  name="nama_director" value={this.state.postBsi.nama_director} placeholder="Nama Director" />
                                                        </div>        
                                                        <div className="col-md-12 m-b-20">
                                                            <input type="number" className="form-control"  onChange={this.hendeleChange}  name="no_hp" value={this.state.postBsi.no_hp} placeholder="No Hp / Wa" />
                                                        </div>   

                                                        <div className="col-md-12 m-b-20">
                                                            <input type="email" className="form-control"  onChange={this.hendeleChange}  name="email" value={this.state.postBsi.email} placeholder="email" />
                                                        </div>  
                                                    </div>
                                             <div className="modal-footer">
                                                <button type="button" className="btn btn-info waves-effect" data-dismiss="modal" onClick={this.hendleSubmit}>Simpan</button>
                                                <button type="button" className="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                                            </div>
                                                </form>
                                            </div>
                                           
                                        </div>
                                    </div>
                                </div>

                                <div id="add-update" className="modal fade in" role="dialog" aria-labelledby="mymodallabel" aria-hidden="true">
                                    <div className="modal-dialog">
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 className="modal-title" id="mymodallabel"> {this.state.isUpdate ? 'Update' : 'Tambah'}  Data</h4> </div>
                                            <div className="modal-body">
                                                <form className="form-horizontal form-material"   >
                                                    <div className="form-group">
                                                        <input type="hidden" value={this.state.postBsi.id} name="id_data" />
                                                       <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control"  onChange={this.hendeleChange} value={this.state.postBsi.kode_bsi}  name="kode_bsi" placeholder="kode" /> </div>
                                                        
                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control"  onChange={this.hendeleChange}  name="nama" value={this.state.postBsi.nama} placeholder="nama" /> </div>
                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control"  onChange={this.hendeleChange}  name="alamat" value={this.state.postBsi.alamat} placeholder="alamat" />
                                                        </div>  
                                                        
                                                         <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control"  onChange={this.hendeleChange}  name="nama_director" value={this.state.postBsi.nama_director} placeholder="Nama Director" />
                                                        </div>        
                                                        <div className="col-md-12 m-b-20">
                                                            <input type="number" className="form-control"  onChange={this.hendeleChange}  name="no_hp" value={this.state.postBsi.no_hp} placeholder="No Hp / Wa" />
                                                        </div>   

                                                        <div className="col-md-12 m-b-20">
                                                            <input type="email" className="form-control"  onChange={this.hendeleChange}  name="email" value={this.state.postBsi.email} placeholder="email" />
                                                        </div>                                                
                                                    </div>
                                                   
                                                    
                                                </form>
                                            </div>
                                            <div className="modal-footer">
                                                <button type="button" className="btn btn-info waves-effect" onClick={this.updateSubmit} data-dismiss="modal">Simpan</button>
                                                <button type="button" className="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

										    </div>

										    
                                            <div className="offset-md-8 col-md-3 ">
                                            <br />
                                                <input type="searech" id="cari" placeholder="Pencarian" />
                                            </div>

                                        </div>
                                        </div>
                                        <div id="collapseOne" className="panel-collapse collapse in">

                                            <div className="panel-body">
                                                <div className="card-body">
                                                    <div className="table-responsive">
                                                        <table id="demo-foo-accordion" className="table table-bordered m-b-0 toggle-arrow-tiny" data-filtering="true" data-paging="true" data-sorting="true">
                                                            <thead>
                                                                <tr className="footable-filtering">
                                                                 	<th>No</th> 
                                                                	<th>Kode</th>
                                                                    <th data-toggle="true"> Nama </th>
                                                                    <th data-hide="phone"> Alamat </th>
                                                                    <th data-hide="all"> Total BSU </th>
                                                                    <th>Nama Director</th>
                                                                    <th>No Hp /WA</th>
                                                                    <th>Email</th>

                                                                    <th  > Edit </th>
                                                                    <th > Hapus </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            {
                                                                 
                                                               Object.keys(this.state.bsi).map((key, index) => {
                                                             
                                                                   if (this.state.bsi[key].status == 1) {

                                                                        return <Tes 
                                                                        key={index} kode={this.state.bsi[key].kode_bsi}
                                                                          nama={this.state.bsi[key].nama }
                                                                         alamat={this.state.bsi[key].alamat} total={this.state.bsi[key].total_bsu}
                                                                         status={this.state.bsi[key].status} romove={this.hendleRemove}
                                                                           update={this.hendeleUpdate} id={key} 
                                                                           nama_director ={this.state.bsi[key].nama_director} email={this.state.bsi[key].email}
                                                                           no_hp ={this.state.bsi[key].no_hp}
                                                                          no={i = i + 1} />
                                                                   }
                                                                  
                                                                    
                                                              })    
                                                            }
                                                            
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                


                   
                    <div className="row">
                    <div className="col-sm-11">
                    <div className="white-box">
                            <div className="card-body">
                                <h4 className="card-title">Data BSI yang di hapus</h4>
                                
                                <div className="panel-group" id="accordion">
                                    <div className="panel panel-default">
                                        <div className="panel-heading">
                                        <div className="row">
										    
										    <div className="col-md-9">
										    	
										    </div>

										    <div className="col col-md-3 offset-md-9">
										    	<input type="searech" id="cari" placeholder="Pencarian" />
										    </div>

                                        </div>
                                        </div>
                                        <div id="collapseOne" className="panel-collapse collapse in">

                                            <div className="panel-body">
                                                <div className="card-body">
                                                    <div className="table-responsive">
                                                        <table id="demo-foo-accordion" className="table table-bordered m-b-0 toggle-arrow-tiny" data-filtering="true" data-paging="true" data-sorting="true">
                                                            <thead>
                                                                <tr className="footable-filtering">
                                                                 	<th>No</th> 
                                                                	<th>Kode</th>
                                                                    <th data-toggle="true"> Nama </th>
                                                                    <th data-hide="phone"> Alamat </th>
                                                                    <th data-hide="all"> Total BSU </th>
                                                                    <th>Nama Director</th>
                                                                    <th>No Hp /WA</th>
                                                                    <th > Kembalikan </th>
                                                                    <th > Hapus </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                               {
                                                                   Object.keys(this.state.bsi).map((key, index) => {
                                                                       if (this.state.bsi[key].status == 0) {
                                                                        
                                                                            return <Tes key={index} 
                                                                                    kode={this.state.bsi[key].kode_bsi} nama={this.state.bsi[key].nama } 
                                                                                    alamat={this.state.bsi[key].alamat} total={this.state.bsi[key].total_bsu}
                                                                                    status={this.state.bsi[key].status} restore={this.hendeleRestore} id={key} 
                                                                                    deletee={this.hendleDeletePer}   no={j = j + 1} 
                                                                                    nama_director ={this.state.bsi[key].nama_director} email={this.state.bsi[key].email}
                                                                                    no_hp ={this.state.bsi[key].no_hp}
                                                                                    />

                                                                       }    
                                                                  })    
                                                                }
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>       
                                </div>
                            </div>
                        </div>
                        </div>
                        


                        
                        
                        
                        </div>


                </div>

               
			</Fragment>
		)
	}
}

export default DataBSI;