import React, {Component, Fragment} from 'react';
import axios from 'axios';

import '../dataMaster/DataMaster.css';
import swal from 'sweetalert';
import { Table, Divider, Tag } from 'antd';
import {APIBSU, APIAPPROV} from  '../../../service/index';
let Petugas = (props) => {

    if (props.status == '2') {
       return (
           <Fragment>
            <tr>
              <td>{props.no}</td>
                <td>{props.nama}</td>
                <td>{props.noHp}</td>
                <td>{props.email}</td>
                <td>{props.kurir} </td>
                <td>{props.level}</td>
                <td> {props.role}    </td>
                <td>
                    Terhapus
                </td>

                <td> 
                    <button  type="button" className="btn-sm btn-success " onClick={() => props.restore(props.id)}><i className="mdi mdi-backup-restore  " > </i></button>
                 
                    </td> 
                    <td> 
                    <button   type="button" className="btn-sm  btn-danger" onClick={() => props.deletee(props.id)}><i className="mdi mdi-delete-forever" ></i></button>

                    </td>  
            </tr>
            </Fragment>
            )
    }else{
        let status = '';
        if (props.status == "0") {
            status = "Belum Aktif"
        }else if(props.status == "1"){
            status = "Aktif"
        }else{
            status = "Terhapus"
        }

    if (props.statusChange == 'aktif') {
        if (props.status == '1') {
            if (props.nama){
            return (
                <tr>
                  <td>{props.no}</td>
                    <td>{props.nama}</td>
                    <td>{props.noHp}</td>
                    <td>{props.email}</td>
                    <td>{props.kurir} </td>
                    <td>{props.level}</td>
                    <td> {props.role}    </td>
                    <td>
                        {status}
                    </td>

                    <td> 

                        <button type="button" className="btn-sm btn-info"  data-target="#add-app"   onClick={() => props.aproval(props.id)}   ><i className="mdi mdi-check-circle"   > </i></button></td>
                        <td>
                        <button   type="button" className="btn-sm  btn-warning" onClick={() => props.romove(props.id)}><i className="mdi mdi-delete-forever " ></i></button>

                        </td>  
                </tr>
            )
        }else{
            return null;
        }
        }else{
            return null
        }
        }else if(props.statusChange == 'blm'){
            if (props.status == '0') {
                return (
                    <tr>
                      <td>{props.no}</td>
                        <td>{props.nama}</td>
                        <td>{props.noHp}</td>
                        <td>{props.email}</td>
                        <td>{props.kurir} </td>
                        <td>{props.level}</td>
                        <td> {props.role}    </td>
                        <td>
                            {status}
                        </td>

                        <td> 
                          <button type="button" className="btn-sm btn-info" onClick={() => props.aproval(props.id)}><i className="mdi mdi-check-circle"  data-target="#add-app"  > </i></button></td>
                        <td>
                        <button   type="button" className="btn-sm  btn-warning" onClick={() => props.romove(props.id)}><i className="mdi mdi-delete-forever " ></i></button>

                            </td>  
                    </tr>
                    )
            }else{
                return null
            }
        }else{
             return (
                <tr>
                  <td>{props.no}</td>
                    <td>{props.nama}</td>
                    <td>{props.noHp}</td>
                    <td>{props.email}</td>
                    <td>{props.kurir} </td>
                    <td>{props.level}</td>
                    <td> {props.role}    </td>
                    <td>
                        {status}
                    </td>

                    <td> 
                       {/* <button type="button"  data-target="#add-app"  className="btn-sm btn-info" onClick={() => props.aproval(props.id)} ><i className="mdi mdi-check-circle"   > </i></button> */}
                       <button type="button" className="btn-sm btn-info" data-toggle="modal" data-target="#add-app" 
                       > <i className="mdi mdi-settings" onClick={() => props.aproval(props.id)}> </i> </button></td>
                     
                       <td>
                        <button   type="button" className="btn-sm  btn-warning" onClick={() => props.romove(props.id)}><i className="mdi mdi-delete-forever " ></i></button>

                        </td>  
                </tr>
            )
        }
    }

    
    }

   


class DataPetugas extends Component{

    state = {
        petugas: [],
        statusChange : 'all',
        bsu : [],
        idPrugas: 0,
        datas: {
            uid: '',
            id_bsu: '',
            
        },
        columns : [
            {
              title: 'No',
              dataIndex: 'no',
              key: 'no',
            },
            {
              title: 'Nama',
              dataIndex: 'nama',
              key: 'nama',
            },
            {
              title: 'No Hp',
              dataIndex: 'noHp',
              key: 'noHp',
            },
            {
              title: 'Email',
              key: 'email',
              dataIndex: 'email',
              
            },
            {
                title: 'Kurir',
                dataIndex: 'kurir',
                key : 'kurir',
            },
            {
                title : 'Level',
                dataIndex: 'level',
                key: 'level'
            },
            {
                title : 'Role',
                dataIndex: 'role',
                key: 'role'
            },
            {
              title: 'Aksi',
              key: 'Aksi',
              render: (text, record) => (
                <span>
                  <a href="javascript:;">Invite {record.name}</a>
                  <Divider type="vertical" />
                  <a href="javascript:;">Delete</a>
                </span>
              ),
            },
          ]
    }

    getApi = () => {
        axios.get('http://35.198.225.173/api/data/petugas')
            .then((res) => {
                this.setState({
                    petugas: res.data
                })
                console.log(res)
        })

        APIBSU.getBSU().then((res) => {
            this.setState({
                bsu: res
            })
        })

    }

    hendeleChange = (event) => {
        this.setState({
            statusChange: event.target.value
        })
    }
    componentDidMount(){
        document.title = "Approval"
        this.getApi()
    }

    hendleApprov = (data) => {
        // swal("Berhasil!!", "Petugas Telah AKtif", "success");
        // console.log(data)
        // axios.get(`http://35.198.225.173/api/data/petugas/approve/${data}`)
        // .then(res => {
        //     this.getApi()
        // })

        
        this.setState({
            idPrugas: data
        }, () => {
            console.log(this.state.idPrugas)
        })

        

    }

    hendeleRestore = (data) => {
       swal("Berhasil!!", "Data Telah DiKembalikan", "success");
         axios.get(`http://35.198.225.173/api/data/petugas/approve/aktif/${data}`)
        .then(res => {
            this.getApi()
        })
    }

    hendleDeletePer = (data) => {
        swal("Berhasil!!", "Data DI Hapus Permanen", "success");
        console.log(data)
        axios.get(`http://35.198.225.173/api/data/petugas/deletep/${data}`)
        .then(res => {
            this.getApi()
        })
    }

    hendeleChange = (event) => {
        // let postPtugas = {...this.state.postBsi};
        // console.log(event.target.value);
        
    
        // postPtugas[event.target.name] = event.target.value

        this.setState({
            datas:{
                uid: this.state.idPrugas,
                id_bsu:  event.target.value,
            }
        })
    }

    hendleSubmit = () => {
        // console.log();
        const datas = {
            bsu : this.state.datas.id_bsu,
            uid: this.state.idPrugas
        }
        console.log(datas);
        if (datas.bsu == '' || datas.uid == '') {
            swal({
                title: "Ops..!",
                text: "Silahkan Pilih Penempatan BSU dulu !!!",
                icon: "error",
              });
        }else{
            APIAPPROV.aktif(datas.uid).then((res) => {

            })
         
             APIAPPROV.approv(datas).then( (res) => {
             
             })
 
        //  this.getApi()
        axios.get('http://35.198.225.173/api/data/petugas')
        .then((res) => {
            this.setState({
                petugas: res.data
            })
            // console.log(res)
        })
        
         this.setState({
             idPrugas: 0,
             datas:{
                 uid: '',
                 id_bsu: '',  
             }
         })
        }


        
           


       
    }

    hendleRemove = (data) => {
        swal("Berhasil!!", "Data DI Hapus Sementara", "success");
        axios.get(`http://35.198.225.173/api/data/petugas/approve/nonaktif/${data}`)
        .then(res => {
            this.getApi()
        })
    }

	render(){
        let i = 0;
        let j = 0;
        // console.log(this.state.petugas);
        
		return (
            <Fragment >
			  <div id="page-wrapper">
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 className="page-title" id ="tulis">Data Member</h4> 
                    </div>
                </div>

               
               </div>
               <div className="container">
               <div className="row">
               <div className="col-sm-11">
               <div className="white-box">
                            <div className="card-body">
                                <h4 className="card-title">Data Member</h4>
                                
                                <div className="panel-group" id="accordion">
                                    <div className="panel panel-default">
                                        <div className="panel-heading">

                                        <div className="row">
                                            <div className="col col-md-4">
                                                 <div className="form-group">
                                                    <label >Katagori</label>
                                                    <select className="form-control" onChange={this.hendeleChange}>
                                                      <option value="all">All</option>
                                                      <option value="blm">Yang Belum Aktif</option>
                                                      <option value="aktif">Aktif</option>
                                                    </select>
                                                  </div>
                                            </div>
                                            
                                        </div>
                                             
                                        </div>

                                        <div id="add-app" className="modal fade in" role="dialog" aria-labelledby="mymodallabel" aria-hidden="true">
                                        <div className="modal-dialog">
                                            <div className="modal-content">
                                                <div className="modal-header">
                                                    <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 className="modal-title" id="mymodallabel">Avvropal</h4> </div>
                                                <div className="modal-body">
                                                    <form className="form-horizontal form-material" >
                                                        <div className="form-group">
                                                           <div className="col-md-12 m-b-20">
                                                            <input type="hidden" value={this.state.idPrugas} name="uid" />
                                                           <label >Pilih BSU</label>
                                                            <select className="form-control" onChange={this.hendeleChange} name="id_bsu" value={this.state.datas.id_bsu}>
                                                                <option value="">PILIH</option>
                                                                {
                                                                    Object.keys(this.state.bsu).map((key, index) => {
                                                                        return <option key={index}  value={key}>{this.state.bsu[key].nama} - {this.state.bsu[key].alamat}  </option>
                                                                    })
                                                                }
                                                                 
                                                               
                                                            </select>
                                                            </div>
                                                            
                                                        </div>
                                                        <div className="modal-footer">
                                                    <button type="button" className="btn btn-info waves-effect" onClick={this.hendleSubmit} data-dismiss="modal">Simpan</button>
                                                    <button type="button" className="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                                                </div>
                                                    </form>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
    



                                        <div id="collapseOne" className="panel-collapse collapse in">
                                            <div className="panel-body">
                                                <div className="card-body">
                                                    <div className="table-responsive">
                                                        <table id="demo-foo-accordion" className="table table-bordered m-b-0 toggle-arrow-tiny" data-filtering="true" data-paging="true" data-sorting="true">
                                                            <thead>
                                                                <tr className="footable-filtering">
                                                                    <th>No</th>
                                                                    <th>Nama</th>
                                                                    <th data-hide="phone"> No Hp </th>
                                                                    <th data-toggle="true">Email </th>
                                                                    <th data-hide="all"> Kurir </th>
                                                                    <th data-hide="all"> Level </th>
                                                                    <th > Role</th>
                                                                    <th > Status </th>
                                                                     <th  > Approv </th>
                                                                    <th > Hapus </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                         
                                                            {
                                                                Object.keys(this.state.petugas).map((key, index) => {
                                                                     if (this.state.petugas[key].status != '2') {
                                                                    return <Petugas key={index} nama={this.state.petugas[key].nama} email={this.state.petugas[key].namaemail} 
                                                                                noHp={this.state.petugas[key].hp} 
                                                                                kurir={this.state.petugas[key].kurir} level={this.state.petugas[key].level}
                                                                                role={this.state.petugas[key].role} status={this.state.petugas[key].status}
                                                                                statusChange={this.state.statusChange} id={key} 
                                                                                aproval={this.hendleApprov}  romove={this.hendleRemove} no={i = i + 1}
                                                                     />  
                                                                     }    
                                                                    
                                                              })    
                                                            }
                                                            </tbody>
                                                        </table>
                                                          
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                        {/* <Table columns={this.state.columns}  /> */}
                        <div className="row">
                    <div className="col-lg-12">
                        <div className="white-box">
                            <h3 className="box-title">Editable with Datatable</h3>
                            <p className="text-muted">Just click on word which you want to change and enter</p>
                            <table className="table table-striped table-bordered" id="editable-datatable">
                                <thead>
                                    <tr>
                                        <th>Rendering engine</th>
                                        <th>Browser</th>
                                        <th>Platform(s)</th>
                                        <th>Engine version</th>
                                        <th>CSS grade</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="1" className="gradeX">
                                        <td>Trident</td>
                                        <td>Internet Explorer 4.0 </td>
                                        <td>Win 95+</td>
                                        <td className="center">4</td>
                                        <td className="center">X</td>
                                    </tr>
                                    <tr id="2" className="gradeC">
                                        <td>Trident</td>
                                        <td>Internet Explorer 5.0</td>
                                        <td>Win 95+</td>
                                        <td className="center">5</td>
                                        <td className="center">C</td>
                                    </tr>
                                    <tr id="3" className="gradeA">
                                        <td>Trident</td>
                                        <td>Internet Explorer 5.5</td>
                                        <td>Win 95+</td>
                                        <td className="center">5.5</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="4" className="gradeA">
                                        <td>Trident</td>
                                        <td>Internet Explorer 6</td>
                                        <td>Win 98+</td>
                                        <td className="center">6</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="5" className="gradeA">
                                        <td>Trident</td>
                                        <td>Internet Explorer 7</td>
                                        <td>Win XP SP2+</td>
                                        <td className="center">7</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="6" className="gradeA">
                                        <td>Trident</td>
                                        <td>AOL browser (AOL desktop)</td>
                                        <td>Win XP</td>
                                        <td className="center">6</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="7" className="gradeA">
                                        <td>Gecko</td>
                                        <td>Firefox 1.0</td>
                                        <td>Win 98+ / OSX.2+</td>
                                        <td className="center">1.7</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="8" className="gradeA">
                                        <td>Gecko</td>
                                        <td>Firefox 1.5</td>
                                        <td>Win 98+ / OSX.2+</td>
                                        <td className="center">1.8</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="9" className="gradeA">
                                        <td>Gecko</td>
                                        <td>Firefox 2.0</td>
                                        <td>Win 98+ / OSX.2+</td>
                                        <td className="center">1.8</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="10" className="gradeA">
                                        <td>Gecko</td>
                                        <td>Firefox 3.0</td>
                                        <td>Win 2k+ / OSX.3+</td>
                                        <td className="center">1.9</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="11" className="gradeA">
                                        <td>Gecko</td>
                                        <td>Camino 1.0</td>
                                        <td>OSX.2+</td>
                                        <td className="center">1.8</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="12" className="gradeA">
                                        <td>Gecko</td>
                                        <td>Camino 1.5</td>
                                        <td>OSX.3+</td>
                                        <td className="center">1.8</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="13" className="gradeA">
                                        <td>Gecko</td>
                                        <td>Netscape 7.2</td>
                                        <td>Win 95+ / Mac OS 8.6-9.2</td>
                                        <td className="center">1.7</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="14" className="gradeA">
                                        <td>Gecko</td>
                                        <td>Netscape Browser 8</td>
                                        <td>Win 98SE+</td>
                                        <td className="center">1.7</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="15" className="gradeA">
                                        <td>Gecko</td>
                                        <td>Netscape Navigator 9</td>
                                        <td>Win 98+ / OSX.2+</td>
                                        <td className="center">1.8</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="16" className="gradeA">
                                        <td>Gecko</td>
                                        <td>Mozilla 1.0</td>
                                        <td>Win 95+ / OSX.1+</td>
                                        <td className="center">1</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="17" className="gradeA">
                                        <td>Gecko</td>
                                        <td>Mozilla 1.1</td>
                                        <td>Win 95+ / OSX.1+</td>
                                        <td className="center">1.1</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="18" className="gradeA">
                                        <td>Gecko</td>
                                        <td>Mozilla 1.2</td>
                                        <td>Win 95+ / OSX.1+</td>
                                        <td className="center">1.2</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="19" className="gradeA">
                                        <td>Gecko</td>
                                        <td>Mozilla 1.3</td>
                                        <td>Win 95+ / OSX.1+</td>
                                        <td className="center">1.3</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="20" className="gradeA">
                                        <td>Gecko</td>
                                        <td>Mozilla 1.4</td>
                                        <td>Win 95+ / OSX.1+</td>
                                        <td className="center">1.4</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="21" className="gradeA">
                                        <td>Gecko</td>
                                        <td>Mozilla 1.5</td>
                                        <td>Win 95+ / OSX.1+</td>
                                        <td className="center">1.5</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="22" className="gradeA">
                                        <td>Gecko</td>
                                        <td>Mozilla 1.6</td>
                                        <td>Win 95+ / OSX.1+</td>
                                        <td className="center">1.6</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="23" className="gradeA">
                                        <td>Gecko</td>
                                        <td>Mozilla 1.7</td>
                                        <td>Win 98+ / OSX.1+</td>
                                        <td className="center">1.7</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="24" className="gradeA">
                                        <td>Gecko</td>
                                        <td>Mozilla 1.8</td>
                                        <td>Win 98+ / OSX.1+</td>
                                        <td className="center">1.8</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="25" className="gradeA">
                                        <td>Gecko</td>
                                        <td>Seamonkey 1.1</td>
                                        <td>Win 98+ / OSX.2+</td>
                                        <td className="center">1.8</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="26" className="gradeA">
                                        <td>Gecko</td>
                                        <td>Epiphany 2.20</td>
                                        <td>Gnome</td>
                                        <td className="center">1.8</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="27" className="gradeA">
                                        <td>Webkit</td>
                                        <td>Safari 1.2</td>
                                        <td>OSX.3</td>
                                        <td className="center">125.5</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="28" className="gradeA">
                                        <td>Webkit</td>
                                        <td>Safari 1.3</td>
                                        <td>OSX.3</td>
                                        <td className="center">312.8</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="29" className="gradeA">
                                        <td>Webkit</td>
                                        <td>Safari 2.0</td>
                                        <td>OSX.4+</td>
                                        <td className="center">419.3</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="30" className="gradeA">
                                        <td>Webkit</td>
                                        <td>Safari 3.0</td>
                                        <td>OSX.4+</td>
                                        <td className="center">522.1</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="31" className="gradeA">
                                        <td>Webkit</td>
                                        <td>OmniWeb 5.5</td>
                                        <td>OSX.4+</td>
                                        <td className="center">420</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="32" className="gradeA">
                                        <td>Webkit</td>
                                        <td>iPod Touch / iPhone</td>
                                        <td>iPod</td>
                                        <td className="center">420.1</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="33" className="gradeA">
                                        <td>Webkit</td>
                                        <td>S60</td>
                                        <td>S60</td>
                                        <td className="center">413</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="34" className="gradeA">
                                        <td>Presto</td>
                                        <td>Opera 7.0</td>
                                        <td>Win 95+ / OSX.1+</td>
                                        <td className="center">-</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="35" className="gradeA">
                                        <td>Presto</td>
                                        <td>Opera 7.5</td>
                                        <td>Win 95+ / OSX.2+</td>
                                        <td className="center">-</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="36" className="gradeA">
                                        <td>Presto</td>
                                        <td>Opera 8.0</td>
                                        <td>Win 95+ / OSX.2+</td>
                                        <td className="center">-</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="37" className="gradeA">
                                        <td>Presto</td>
                                        <td>Opera 8.5</td>
                                        <td>Win 95+ / OSX.2+</td>
                                        <td className="center">-</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="38" className="gradeA">
                                        <td>Presto</td>
                                        <td>Opera 9.0</td>
                                        <td>Win 95+ / OSX.3+</td>
                                        <td className="center">-</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="39" className="gradeA">
                                        <td>Presto</td>
                                        <td>Opera 9.2</td>
                                        <td>Win 88+ / OSX.3+</td>
                                        <td className="center">-</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="40" className="gradeA">
                                        <td>Presto</td>
                                        <td>Opera 9.5</td>
                                        <td>Win 88+ / OSX.3+</td>
                                        <td className="center">-</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="41" className="gradeA">
                                        <td>Presto</td>
                                        <td>Opera for Wii</td>
                                        <td>Wii</td>
                                        <td className="center">-</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="42" className="gradeA">
                                        <td>Presto</td>
                                        <td>Nokia N800</td>
                                        <td>N800</td>
                                        <td className="center">-</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="43" className="gradeA">
                                        <td>Presto</td>
                                        <td>Nintendo DS browser</td>
                                        <td>Nintendo DS</td>
                                        <td className="center">8.5</td>
                                        <td className="center">C/A<sup>1</sup></td>
                                    </tr>
                                    <tr id="44" className="gradeC">
                                        <td>KHTML</td>
                                        <td>Konqureror 3.1</td>
                                        <td>KDE 3.1</td>
                                        <td className="center">3.1</td>
                                        <td className="center">C</td>
                                    </tr>
                                    <tr id="45" className="gradeA">
                                        <td>KHTML</td>
                                        <td>Konqureror 3.3</td>
                                        <td>KDE 3.3</td>
                                        <td className="center">3.3</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="46" className="gradeA">
                                        <td>KHTML</td>
                                        <td>Konqureror 3.5</td>
                                        <td>KDE 3.5</td>
                                        <td className="center">3.5</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="47" className="gradeX">
                                        <td>Tasman</td>
                                        <td>Internet Explorer 4.5</td>
                                        <td>Mac OS 8-9</td>
                                        <td className="center">-</td>
                                        <td className="center">X</td>
                                    </tr>
                                    <tr id="48" className="gradeC">
                                        <td>Tasman</td>
                                        <td>Internet Explorer 5.1</td>
                                        <td>Mac OS 7.6-9</td>
                                        <td className="center">1</td>
                                        <td className="center">C</td>
                                    </tr>
                                    <tr id="49" className="gradeC">
                                        <td>Tasman</td>
                                        <td>Internet Explorer 5.2</td>
                                        <td>Mac OS 8-X</td>
                                        <td className="center">1</td>
                                        <td className="center">C</td>
                                    </tr>
                                    <tr id="50" className="gradeA">
                                        <td>Misc</td>
                                        <td>NetFront 3.1</td>
                                        <td>Embedded devices</td>
                                        <td className="center">-</td>
                                        <td className="center">C</td>
                                    </tr>
                                    <tr id="51" className="gradeA">
                                        <td>Misc</td>
                                        <td>NetFront 3.4</td>
                                        <td>Embedded devices</td>
                                        <td className="center">-</td>
                                        <td className="center">A</td>
                                    </tr>
                                    <tr id="52" className="gradeX">
                                        <td>Misc</td>
                                        <td>Dillo 0.8</td>
                                        <td>Embedded devices</td>
                                        <td className="center">-</td>
                                        <td className="center">X</td>
                                    </tr>
                                    <tr id="53" className="gradeX">
                                        <td>Misc</td>
                                        <td>Links</td>
                                        <td>Text only</td>
                                        <td className="center">-</td>
                                        <td className="center">X</td>
                                    </tr>
                                    <tr id="54" className="gradeX">
                                        <td>Misc</td>
                                        <td>Lynx</td>
                                        <td>Text only</td>
                                        <td className="center">-</td>
                                        <td className="center">X</td>
                                    </tr>
                                    <tr id="55" className="gradeC">
                                        <td>Misc</td>
                                        <td>IE Mobile</td>
                                        <td>Windows Mobile 6</td>
                                        <td className="center">-</td>
                                        <td className="center">C</td>
                                    </tr>
                                    <tr id="56" className="gradeC">
                                        <td>Misc</td>
                                        <td>PSP browser</td>
                                        <td>PSP</td>
                                        <td className="center">-</td>
                                        <td className="center">C</td>
                                    </tr>
                                    <tr id="57" className="gradeU">
                                        <td>Other browsers</td>
                                        <td>All others</td>
                                        <td>-</td>
                                        <td className="center">-</td>
                                        <td className="center">U</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Rendering engine</th>
                                        <th>Browser</th>
                                        <th>Platform(s)</th>
                                        <th>Engine version</th>
                                        <th>CSS grade</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>

                        {/* ====== */}

                         <div className="white-box">
                            <div className="card-body">
                                <h4 className="card-title">Data Member Yang Dihapus</h4>
                                
                                <div className="panel-group" id="accordion">
                                    <div className="panel panel-default">
                                        <div className="panel-heading">

                                                                                
                                        </div>
                                        <div id="collapseOne" className="panel-collapse collapse in">
                                            <div className="panel-body">
                                                <div className="card-body">
                                                    <div className="table-responsive">
                                                        <table id="demo-foo-accordion" className="table table-bordered m-b-0 toggle-arrow-tiny" data-filtering="true" data-paging="true" data-sorting="true">
                                                            <thead>
                                                                <tr className="footable-filtering">
                                                                    <th>No</th>
                                                                    <th>Nama</th>
                                                                    <th data-hide="phone"> No Hp </th>
                                                                    <th data-toggle="true">Email </th>
                                                                    <th data-hide="all"> Kurir </th>
                                                                    <th data-hide="all"> Level </th>
                                                                    <th > Role</th>
                                                                    <th > Status </th> 
                                                                    <th > Kembalikan </th>
                                                                    <th > Hapus </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                               {
                                                                Object.keys(this.state.petugas).map((key, index) => {
                                                                     if (this.state.petugas[key].status == '2') {
                                                                        
                                                                        return <Petugas key={index} nama={this.state.petugas[key].nama} email={this.state.petugas[key].namaemail} 
                                                                                    noHp={this.state.petugas[key].hp} 
                                                                                    kurir={this.state.petugas[key].kurir} level={this.state.petugas[key].level}
                                                                                    role={this.state.petugas[key].role} status={this.state.petugas[key].status}
                                                                                    id={key} restore={this.hendeleRestore} deletee={this.hendleDeletePer}  no={j = j + 1}
                                                                                   
                                                                         />      
                                                                    }
                                                                    
                                                              })    
                                                            }
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        </div>
                        </div>
                    </div>

                    
               {/* <footer className="footer text-center"> 2019 &copy; Sampun </footer> */}
        </div>
        </Fragment>
		)
	}
}

export default DataPetugas;