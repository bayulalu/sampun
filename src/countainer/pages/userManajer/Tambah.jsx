import React, {Component, Fragment} from 'react';
import '../dataMaster/DataMaster.css';
import {APIAPPROV, ADDADMIN} from '../../../service/index';
let IsiTabel = (props) => {
    return (
           <tr>
                <td>{props.no}</td>
                <td>{props.phone}</td>
                <td>{props.nama}</td>
                <td>{props.email}</td>
                <td>{props.level}</td>
                <td>
                <button type="button" className="btn-sm btn-info" data-toggle="modal" data-target="#add-update" 
                > <i className="mdi mdi-settings"> </i> </button>  </td>
              <td>
                <button type="button" className="btn-sm  btn-warning" > <i className="mdi mdi-delete"></i> </button> </td>
       
            </tr>
    )
}

class Tambah extends Component{
    state = {
        admin: [],
        petugas: [],
        tambahAdmin : {
            namaId: '',
            jabatan: ''
        }

    }

    getAdmin = () => {
        ADDADMIN.admin().then(res => {
            this.setState({
                admin: res
            })
        })
    }

    componentDidMount(){
        document.title = "Tambah Admin"

        APIAPPROV.aktif().then(res => {
            this.setState({
                petugas: res
            })
        })

        this.getAdmin()
     

    }

    hendeleChange = (event) => {
      
         let postPtugas = {...this.state.tambahAdmin};
         console.log(event.target.value);
        
    
         postPtugas[event.target.name] = event.target.value
 
         this.setState({
            tambahAdmin: postPtugas
         })
    }

    hendleSubmit = () => {
        if(this.state.tambahAdmin.namaId != '' && this.state.tambahAdmin.jabatan){
           
            if(this.state.admin[this.state.tambahAdmin.namaId] == undefined){
                    
                let admin = this.state.petugas[this.state.tambahAdmin.namaId];
                
                
                const data = {
                    uid : this.state.tambahAdmin.namaId,
                    nama: admin.nama,
                    phone: admin.hp,
                    email: admin.namaemail,
                    password: '12345',
                    level: this.state.tambahAdmin.jabatan
                }
                ADDADMIN.addAdmin(data).then(res => {
                    console.log(res);
                    this.getAdmin()
                    this.setState({
                        tambahAdmin : {
                            namaId: '',
                            jabatan: ''
                        }
                    })
                }) 

            } else{
                alert("user udah terdaftar !!!");
            }
          
        }else{
            alert("Data Harus Lengkap !!!");
        }

    }

    render(){
        let i = 0;
        return(
            <Fragment >
            <div id="page-wrapper">
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 className="page-title" >Tambah Admin Baru</h4> 
                    </div>
                </div>
               </div>
               <div className="container">
               <div className="row">
               <div className="col-sm-11">
               <div className="white-box">
                            <div className="card-body">
                                <h4 className="card-title">Data Admin</h4>
                                
                                <div className="panel-group" id="accordion">
                                    <div className="panel panel-default">
                                        <div className="panel-heading">
                                        <div className="row">
                                        <div className="col-md-9">
                                             <button type="button" className=" btn btn-primary btn-rounded m-t-10 float-right tambahData" data-toggle="modal" data-target="#add-contact" onClick={this.tambah}>Tambah Data</button>


                            <div id="add-contact" className="modal fade in" role="dialog" aria-labelledby="mymodallabel" aria-hidden="true">
                                <div className="modal-dialog">
                                    <div className="modal-content">
                                        <div className="modal-header">
                                            <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 className="modal-title" id="mymodallabel">Tambah Data Admin</h4> </div>
                                        <div className="modal-body">
                                            <form className="form-horizontal form-material" >
                                            <div className="form-group">
                                            <div className="col-md-12 m-b-20">
                                            <label >Pilih Petugas</label>
                                             <select className="form-control" name="namaId" value={this.state.tambahAdmin.namaId} onChange={this.hendeleChange}>
                                                 <option value="">PILIH</option>
                                                 {
                                                    Object.keys(this.state.petugas).map((key, index) => {
                                                        return <option key={index}  value={key}>{this.state.petugas[key].nama}  </option>
                                                    })
                                                }
                                                
                                             </select>
                                             </div>
                                             
                                         </div>

                                         <div className="form-group">
                                         <div className="col-md-12 m-b-20">
                                         <label >Hak Akses</label>
                                          <select className="form-control" name="jabatan" onChange={this.hendeleChange} value={this.state.tambahAdmin.jabatan}>
                                              <option value="">PILIH</option>
                                              <option value="1">Super User</option>
                                              <option value="2">BSU</option>
                                              <option value="3">BSI</option>
                                          </select>
                                          </div>
                                          
                                      </div>
                                            
                                            <div className="modal-footer">
                                            <button type="button" className="btn btn-info waves-effect" onClick={this.hendleSubmit} data-dismiss="modal">Simpan</button>
                                            <button type="button" className="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                                        </div>
                                            </form>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                                        </div>

                                        <div className="offset-md-9 col-md-3 ">
                                        <br />
                                            <input type="searech" id="cari" placeholder="Pencarian" />
                                        </div>

                                    </div>
                                    
                                             
                                        </div>
                                        <div id="collapseOne" className="panel-collapse collapse in">
                                            <div className="panel-body">
                                                <div className="card-body">
                                                    <div className="table-responsive">
                                                        <table id="demo-foo-accordion" className="table table-bordered m-b-0 toggle-arrow-tiny" data-filtering="true" data-paging="true" data-sorting="true">
                                                            <thead>
                                                                <tr className="footable-filtering">
                                                                    <th>No</th>
                                                                    <th>Hp</th>
                                                                    <th>Nama</th>
                                                                    <th data-toggle="true"> Email </th>
                                                                    <th> Level</th>
                                                                    <th>Edit</th>
                                                                    <th>Hapus</th>
                                                                   
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                             {
                                                                  
                                                               
                                                                Object.keys(this.state.admin).map((key, index) => {
                                                                  return <IsiTabel key={index} email={this.state.admin[key].email}
                                                                  nama={this.state.admin[key].nama}
                                                                                    
                                                                                    phone={this.state.admin[key].phone} 
                                                                                    nama={this.state.admin[key].nama}
                                                                                    level={this.state.admin[key].level} 
                                                                                
                                                                                    no={i= i + 1}
                                                                  />    
                                                                   
                                                                  })  
                                                              
                                                             }
                                                               
                                                               
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>  
                        </div>
                        </div>
                    </div>  
              
        </div>
            
            </Fragment>
        )
    }
}


export default Tambah;