import React, {Component, Fragment} from 'react';
import './Approval.css';

import '../dataMaster/DataMaster.css';
import {NASABAH} from '../../../service/index';

let IsiTabel = (props) => {

    let data = {
        id: props.id,
        nomor_rek : props.noReq,
        hp : props.hp,
        nama: props.nama,
        jk : props.jk,
        pekerjaan: props.pekerjaan,
        alamat : props.alamat,
        penghasilan: props.penghasilan,
        jml_anggota: props.jml_anggota,
        ttl : props.ttl
   }
 
    return (
           <tr>
                <td>{props.no}</td>
                <td>{props.noReq}</td>
                <td>{props.hp}</td>
                <td>{props.nama}</td>
                <td>{props.jk}</td>
                <td>{props.pekerjaan}</td>
                <td>{props.alamat}</td>
                <td>{props.penghasilan}</td>
                <td>{props.jml_anggota}</td>
                <td>{props.ttl}</td>
              
             <td>   <button type="button" className="btn-sm btn-info" data-toggle="modal" data-target="#add-update" 
                    onClick={() => props.update(data)} > <i className="mdi mdi-settings"> </i> </button>  </td>
              <td><button type="button" onClick={() => props.deletee(props.id)}  className="btn-sm  btn-warning" > <i className="mdi mdi-delete"></i> </button> </td>
       
                
                

               
            </tr>
    )
}


class Nasabah extends Component{

    state = {
        nasabah: [],
        postNasabah :{
            id: '',
            nomor_rek : '',
            hp : '',
            nama: '',
            jk : '',
            pekerjaan: '',
            alamat : '',
            penghasilan: '',
            jml_anggota: '',
            ttl : ''
        }
    }

    getData = () => {
        NASABAH.nasabah().then(res => {
            this.setState({
                nasabah: res
            })
        }) 
    }

    
    updateApi = () => {
        const nasabah = {
            id : this.state.postNasabah.id,
            nama : this.state.postNasabah.nama,
            alamat : this.state.postNasabah.alamat,
            jk : this.state.postNasabah.jk,
            hp : this.state.postNasabah.hp,
            nomor_rek : this.state.postNasabah.nomor_rek,
            tgl_lahir : this.state.postNasabah.ttl,
            penghasilan : this.state.postNasabah.penghasilan,
            pekerjaan : this.state.postNasabah.pekerjaan,
            jml_anggota : this.state.postNasabah.jml_anggota,
            ttl: this.state.postNasabah.ttl
        }
        
        NASABAH.putNasabah(nasabah).then((res) => {
            this.getData()
        })
     }

     componentDidMount(){
        document.title = "Nasabah"
        this.getData()
    }

    hendeleUpdate = (data) => {
        this.setState({
            postNasabah: data
        })
        
    }
    
    hendleRemove = (data) => {
        console.log(data);
        
        NASABAH.permanenNasabah(data).then((res) => {
            console.log(res);
            
            this.getData()
        })
    }

    hendeleChange = (event) => {
        let postNasabahNew = {...this.state.postNasabah};
        postNasabahNew[event.target.name] = event.target.value

        this.setState({
            postNasabah: postNasabahNew
        })
    }

    hendleSubmit = (event) => {
       this.updateApi();
       this.getData();
       this.setState({
        postNasabah : {
            id: '',
            nomor_rek : '',
            hp : '',
            nama: '',
            jk : '',
            pekerjaan: '',
            alamat : '',
            penghasilan: '',
            jml_anggota: '',
            ttl : ''
        }
       })


        
    }




	render(){
        
    let i = 0;
		return (
            <Fragment >
			  <div id="page-wrapper">
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 className="page-title" id ="tulis">  Data Nasabah</h4> 
                    </div>
                </div>
               </div>
               <div className="container">
               <div className="row">
               <div className="col-sm-11">
               <div className="white-box">
                            <div className="card-body">
                                <h4 className="card-title">Data Nasabah</h4>
                                
                                <div className="panel-group" id="accordion">
                                    <div className="panel panel-default">
                                        <div className="panel-heading">

                                        <div id="add-update" className="modal fade in" role="dialog" aria-labelledby="mymodallabel" aria-hidden="true" >
                                    <div className="modal-dialog">
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 className="modal-title" id="mymodallabel">Edit Data</h4> </div>
                                            <div className="modal-body">
                                                <form className="form-horizontal form-material" onSubmit={this.hendleSubmit} >
                                                    <div className="form-group">
                                                        <input type="hidden" value={this.state.postNasabah.id} name="id" />
                                                       <div className="col-md-12 m-b-20">
                                                            <input type="number" className="form-control"  onChange={this.hendeleChange} value={this.state.postNasabah.nomor_rek}  name="nomor_rek" placeholder="No Rek" /> </div>
                                                        
                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control"  onChange={this.hendeleChange}  name="hp" value={this.state.postNasabah.hp} placeholder="No Hp" /> </div>
                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control"  onChange={this.hendeleChange}  name="nama" value={this.state.postNasabah.nama} placeholder="Nama" /> </div>                                                        
                                                            <div className="col-md-12 m-b-20">
                                                            <label >Jenis Kelamin</label>
                                                            <select className="form-control" name="jk" value={this.state.postNasabah.jk}  onChange={this.hendeleChange}>
                                                            <option value="">Pilih</option>
                                                            <option value="Laki-Laki">Laki- Laki</option>
                                                            <option valu="Perempuanz">Perempuan</option>
                                                            </select>
                                                            </div>        
                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control"  onChange={this.hendeleChange}  name="pekerjaan" value={this.state.postNasabah.pekerjaan} placeholder="Pekerjaan" />
                                                        </div>   

                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control"  onChange={this.hendeleChange}  name="penghasilan" value={this.state.postNasabah.penghasilan} placeholder="Penghasilan" />
                                                        </div>  
                                                        <div className="col-md-12 m-b-20">
                                                        <input type="text" className="form-control"  onChange={this.hendeleChange}  name="jml_anggota" value={this.state.postNasabah.jml_anggota} placeholder="Jmlh Anggota Keluarga" />
                                                        </div>  
                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control"  onChange={this.hendeleChange}  name="ttl" value={this.state.postNasabah.ttl} placeholder="TTL" />
                                                        </div>


                                                    </div>
                                             <div className="modal-footer">
                                                <button type="button" className="btn btn-info waves-effect" data-dismiss="modal" onClick={this.hendleSubmit}>Simpan</button>
                                                <button type="button" className="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                                            </div>
                                                </form>
                                            </div>
                                           
                                        </div>
                                    </div>
                                </div>
                                    
                                        </div>
                                        <div id="collapseOne" className="panel-collapse collapse in">
                                            <div className="panel-body">
                                                <div className="card-body">
                                                    <div className="table-responsive">
                                                        <table id="demo-foo-accordion" className="table table-bordered m-b-0 toggle-arrow-tiny" data-filtering="true" data-paging="true" data-sorting="true">
                                                            <thead>
                                                                <tr className="footable-filtering">
                                                                    <th>No</th>
                                                                    <th>No Rek</th>
                                                                    <th>No Hp</th>
                                                                    <th data-toggle="true"> Nama </th>
                                                                    <th> JK </th>
                                                                    <th>Pekerjaan</th>
                                                                    <th>Alamat</th>
                                                                    <th>Penghasilan</th>
                                                                    <th>Jmlh Anggota Keluarga</th>
                                                                    <th>TTL</th>
                                                                    <th>Edit</th>
                                                                    <th>Hapus</th>
                                                                   
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                             {
                                                               
                                                                Object.keys(this.state.nasabah).map((key, index) => {
                                                                  return <IsiTabel key={key} alamat={this.state.nasabah[key].alamat}
                                                                                    hp={this.state.nasabah[key].hp}
                                                                                    jk={this.state.nasabah[key].jk} 
                                                                                    pekerjaan={this.state.nasabah[key].pekerjaan} 
                                                                                    nama={this.state.nasabah[key].nama}
                                                                                    penghasilan={this.state.nasabah[key].penghasilan} 
                                                                                    noReq={this.state.nasabah[key].nomorrek }
                                                                                    jml_anggota={this.state.nasabah[key].jml_anggota}
                                                                                    ttl= {this.state.nasabah[key].ttl}
                                                                                    no={i= i + 1} update={this.hendeleUpdate}
                                                                                    id={key} deletee={this.hendleRemove}

                                                                  />    
                                                                   
                                                                  })  
                                                              
                                                             }
                                                               
                                                               
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>  

                        {/* kedua */}
                        {/* <div className="white-box">
                            <div className="card-body">
                                <h4 className="card-title">Data Nasabah Yang Terhapus</h4>
                                
                                <div className="panel-group" id="accordion">
                                    <div className="panel panel-default">
                                        <div className="panel-heading">

                                    
                                        </div>
                                        <div id="collapseOne" className="panel-collapse collapse in">
                                            <div className="panel-body">
                                                <div className="card-body">
                                                    <div className="table-responsive">
                                                        <table id="demo-foo-accordion" className="table table-bordered m-b-0 toggle-arrow-tiny" data-filtering="true" data-paging="true" data-sorting="true">
                                                            <thead>
                                                                <tr className="footable-filtering">
                                                                    <th>No</th>
                                                                    <th>No Rekening</th>
                                                                    <th>No Hp</th>
                                                                    <th data-toggle="true"> Nama </th>
                                                                    <th> JK</th>
                                                                    <th>Pekerjaan</th>
                                                                    <th>Penghasilan</th>
                                                                    <th>Jumlah Anggota Keluarga</th>
                                                                    <th>TTL</th>
                                                                    <th>Kembalikan</th>
                                                                    <th>Hapus</th>
                                                                   
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                             {
                                                               
                                                                Object.keys(this.state.nasabah).map((key, index) => {
                                                                  return <IsiTabel alamat={this.state.nasabah[key].alamat}
                                                                                    hp={this.state.nasabah[key].hp}
                                                                                    jk={this.state.nasabah[key].jk} 
                                                                                    pekerjaan={this.state.nasabah[key].pekerjaan} 
                                                                                    nama={this.state.nasabah[key].nama}
                                                                                    penghasilan={this.state.nasabah[key].penghasilan} 
                                                                                    noReq={this.state.nasabah[key].nomorrek }
                                                                                    jml_anggota={this.state.nasabah[key].jml_anggota}
                                                                                    ttl= {this.state.nasabah[key].ttl}
                                                                                    no={i= i + 1} 

                                                                  />    
                                                                   
                                                                  })  
                                                              
                                                             }
                                                               
                                                               
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>  */}
                        </div>
                        </div>
                    </div>  
              
        </div>
        </Fragment>
		)
	}
}

export default Nasabah;