import React, {Component, Fragment} from 'react';
import './Approval.css';
import axios from 'axios';

let IsiTabel = (props) => {
    return (
           <tr>
                <td>{props.no}</td>
                <td>{props.noReq}</td>
                <td>{props.hp}</td>
                <td>{props.nama}</td>
                <td>{props.jk}</td>
                <td>{props.pekerjaan}</td>
                <td>{props.penghasilan}</td>
                <td>Hapus</td>
                <td>Edit</td>
                

               
            </tr>
    )
}


class Nasabah extends Component{

    state = {
        nasabah: [],
    }

     componentDidMount(){
        document.title = "Nasabah"

        axios.get('https://sampun.alphacstudios.com/api/data/nasabah')
        .then(res => {
            this.setState({
                nasabah: res.data
            })

        })

    }
	render(){
 let i = 0;
		return (
			  <div id="page-wrapper">
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 className="page-title">Data Nasabah</h4> 
                    </div>
                </div>
               </div>

               <div className="white-box">
                            <div className="card-body">
                                <h4 className="card-title">Data Nasabah</h4>
                                
                                <div className="panel-group" id="accordion">
                                    <div className="panel panel-default">
                                        <div className="panel-heading">

                                    
                                             <div id="add-contact" className="modal fade in" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div className="modal-dialog">
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 className="modal-title" id="myModalLabel">Tambah Data</h4> </div>
                                            <div className="modal-body">
                                                <from className="form-horizontal form-material">
                                                    <div className="form-group">
                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control" placeholder="Kode" /> </div>
                                                        
                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control" placeholder="Nama" /> </div>
                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control" placeholder="Alamat" /> </div>
                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control" placeholder="Jenis Kelamin" /> </div>
                                                        
                                                    </div>
                                                </from>
                                            </div>
                                            <div className="modal-footer">
                                                <button type="button" className="btn btn-info waves-effect" data-dismiss="modal">Simpan</button>
                                                <button type="button" className="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                        </div>
                                        <div id="collapseOne" className="panel-collapse collapse in">
                                            <div className="panel-body">
                                                <div className="card-body">
                                                    <div className="table-responsive">
                                                        <table id="demo-foo-accordion" className="table table-bordered m-b-0 toggle-arrow-tiny" data-filtering="true" data-paging="true" data-sorting="true">
                                                            <thead>
                                                                <tr className="footable-filtering">
                                                                    <th>No</th>
                                                                    <th>No Rekening</th>
                                                                    <th>No Hp</th>
                                                                    <th data-toggle="true"> Nama </th>
                                                                    <th> Jenis - Kelamin</th>
                                                                    <th>Pekerjaan</th>
                                                                    <th>Penghasilan</th>
                                                                    <th>Edit</th>
                                                                    <th>Hapus</th>
                                                                   
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                             {
                                                               
                                                                Object.keys(this.state.nasabah).map((key, index) => {
                                                                  return <IsiTabel alamat={this.state.nasabah[key].alamat}
                                                                                    hp={this.state.nasabah[key].hp}
                                                                                    jk={this.state.nasabah[key].jk} 
                                                                                    pekerjaan={this.state.nasabah[key].pekerjaan} 
                                                                                    nama={this.state.nasabah[key].nama}
                                                                                    penghasilan={this.state.nasabah[key].penghasilan} 
                                                                                    noReq={this.state.nasabah[key].nomorrek }
                                                                                    no={i= i + 1}
                                                                  />    
                                                                   
                                                                  })  
                                                              
                                                             }
                                                               
                                                               
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>  
              
        </div>
		)
	}
}

export default Nasabah;