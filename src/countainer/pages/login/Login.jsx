import React, {Component, Fragment} from 'react';
import './Login.css';
import {withAuth} from '../../../context/AuthContextProvider';
import swal from 'sweetalert';

import { Route, Redirect } from 'react-router-dom';
class Login extends Component{
    state = {
        no_hp: '',
        password : ''
    }

    hendleChange = (event) => {
        const {name, value} = event.target
        this.setState({
            [name] : value
        })
    }

    hendleSubmit = (event) => {
        
        event.preventDefault();
        if(this.state.no_hp !== '' && this.state.password != ''){
            this.setState({
                no_hp: this.state.no_hp.replace('0', '+62')
            }, () => {
                this.props.login(this.state)
            })
            
        }else{
            swal({
                title: "Ops..!",
                text: "Pastikan Anda Telah Mengisi No HP Dan Kata Sandi!!",
                icon: "error",
              });
        }
     
        
    }


    render(){
        if(this.props.isLoggedIn){
            
            return <Redirect push to='/dashboard' />
        }
        return(
            <Fragment>
                <div id="wrapper" className="login-register">
              <section className="login-box login-sidebar">
              
                <div className="white-box">
                <form className="form-horizontal form-material" id="loginform" onSubmit={this.hendleSubmit}>
                    <a href="javascript:void(0)" className="text-center db"><img src="../plugins/images/logo.png" alt="Home" width="100" /><br/><img src="../plugins/images/sampun.png" width="150" alt="Home" /></a>  
                    
                    <div className="form-group m-t-40">
                    <div className="col-xs-12">
                        <input className="form-control" type="number" name="no_hp" value={this.state.no_hp} onChange={this.hendleChange} placeholder="No Hp" />
                    </div>
                    </div>
                    <div className="form-group">
                    <div className="col-xs-12">
                        <input className="form-control" type="password" name="password" value={this.state.password} onChange={this.hendleChange} placeholder="Kata Sandi" />
                    </div>
                    </div>
                    <div className="form-group">
                
                    <div className="form-group text-center m-t-20">
                    <div className="col-xs-12">
                        <button className="btn btn-info btn-login btn-lg btn-block text-uppercase waves-effect waves-light" value="submit" type="submit">Masuk</button>
                    </div>
                    </div>
                    </div>
                    
                </form>
               
            </div>
            </section>
            </div>
            </Fragment>
        )
    }
}

export default withAuth(Login);