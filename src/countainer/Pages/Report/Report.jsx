import React, {Component, Fragment} from 'react';
import { DatePicker, message } from "antd";
import 'antd/dist/antd.css';
import moment from 'moment';
// import Waktu from './Waktu';

const { RangePicker } = DatePicker;

const dateFormat = 'DD/mm/YYYY';

class ReportMember extends Component{
    
    render(){
        console.log(this.props.data)
        if(this.props.data == true){
            return (
                          <div className="white-box">
                            <div className="card-body">
                                <h4 className="card-title">Member</h4>
                                
                                <div className="panel-group" id="accordion">
                                    <div className="panel panel-default">
                                        <div className="panel-heading">
                                         <div className="row">
                                        <div className="col col-md-8">
                                                <RangePicker
                                                    placeholder={['Awal', ' Sampai']}
                                                      format={dateFormat}
                                                />
                                               &nbsp; <button className="btn btn-info">Cari</button>
                                        </div>
                                        <div className="col col-md-4">
                                             <input text="text" placeholder="Cari" />   
                                        </div>
                                        </div>
                                       </div>
                                        <div id="collapseOne" className="panel-collapse collapse in">
                                            <div className="panel-body">
                                                <div className="card-body">
                                                    <div className="table-responsive">
                                                        <table id="demo-foo-accordion" className="table table-bordered m-b-0 toggle-arrow-tiny" data-filtering="true" data-paging="true" data-sorting="true">
                                                            <thead>
                                                                <tr className="footable-filtering">
                                                                    <th data-toggle="true"> Nama </th>
                                                                    <th> Jenis Kelamin</th>
                                                                    <th data-hide="phone"> Alamat </th>
                                                                    <th data-hide="all"> Tanggal Daftar </th>
                                                                    <th data-hide="all"> Report</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Bayu</td>
                                                                    <td>Laki - Laki</td>
                                                                    <td>Lotim</td>
                                                                    <td>3 April 2019</td>
                                                                    <td><button type="button" className="btn btn-primary">Print</button></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Crezh</td>
                                                                    <td>Laki - Laki</td>
                                                                    <td>Lotim</td>
                                                                    <td>3 April 2019</td>
                                                                    <td><button type="button" className="btn btn-primary">Print</button></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Toto</td>
                                                                    <td>Laki - Laki </td>
                                                                    <td>Lobar</td>
                                                                    <td>19 Apr 2019</td>
                                                                    <td><button type="button" className="btn btn-primary">Print</button></td>
                                                                </tr>
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
 
        )    
        }else{
            return null
        }
        
    }
}


class Report extends Component{

    state = {
        member : false
    }

    componentDidMount(){
        document.title = "Report"
    }

     handleSelect = (event) => {
        let data = event.target.value;
        // console.log(data)

        if(data == 'Member'){
            this.setState({
                member : true
            })
        }else{
            this.setState({
                member : false
            })
        }
        
  
    }

	render(){
		return (
			 <Fragment>
                 <div id="page-wrapper">
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 className="page-title">Report</h4> 
                    </div>
                </div>
                <div className="row">
                <div className="col col-lg-5">
                    
                    <div className="form-group">
                    <label >Report</label>
                    <select className="form-control" onChange={this.handleSelect} >
                      <option >Pilih</option>
                      <option>Semua BSU</option>
                      <option>Sema BSI</option>
                      <option>Nasabah</option>
                    </select>
                    </div>
                </div>
                
                </div>

               </div>

                    <ReportMember data={this.state.member} />


               {/* <footer classNameName="footer text-center"> 2019 &copy; Sampun </footer> */}
        </div>
            </Fragment>
		)
	}
}


export default Report;