import React, {Component, Fragment} from 'react';
import './Dasboard.css';
import { DatePicker, message } from "antd";
import 'antd/dist/antd.css';
import Loadable from 'react-loadable';
import {APIDAS} from '../../../service';

const { RangePicker } = DatePicker;

const dateFormat = 'DD/mm/YYYY';

function Loading() {
    return <h3>Sampun Loding julu</h3>;
  }
  
  const Waktu = Loadable({
    loader: () => import('./Waktu'),
    loading: Loading,
  });


class Dashboard extends Component{
    state = {
        date : [],
        dataBulan : '',
        totalNasabah : 0
    }

    componentDidMount(){
        document.title = "Dashboard"
        this.getTotal()
    }

    getTotal = () =>  {
        APIDAS.totalNasabah().then(res => {
            this.setState({
                totalNasabah : res
            })
        })

       
    }


    handleChange = (event, date) => {
        let data = date
        this.setState({
            date: data
        })
    }

    handleSelect = (event) => {
        let data = event.target.value;
        // console.log(data)
        this.setState({
            dataBulan : data
        });

  
    }

    
    render(){
        return(
            <Fragment>
                 <div id="page-wrapper">
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 className="page-title">Dashboard</h4> 
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-6 col-sm-6 col-xs-12">
                        <div className="white-box">
                            <h3 className="box-title">Total Nasabah</h3>
                            <ul className="list-inline two-part">
                                <li><i className="icon-people text-info"></i></li>
                                <li className="text-right"><span className="counter">{this.state.totalNasabah}</span></li>
                            </ul>
                        </div>
                    </div>

                    <div className="col-lg-6 col-sm-6 col-xs-12">
                        <div className="white-box">
                            <h3 className="box-title">Total Register Nasabah Harian</h3>
                            <ul className="list-inline two-part">
                                <li><i className="icon-people text-info"></i></li>
                                <li className="text-right"><span className="counter" >0</span></li>
                            </ul>
                        </div>
                    </div>
                    </div>
               <div id="jarak"></div>
                  <div className="row">
                        
                    <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div className="white-box">
                            
                        <div className="row">

                            <div className="form-group">
                                <label >Pilih Data</label>
                                <select className="form-control" id="exampleFormControlSelect1" onChange={this.handleSelect}>
                                <option >Pilih</option>
                                <option>Hari</option>
                                <option>Bulan</option>
                                <option>Tahun</option>
                                </select>
                            </div>
                        </div> 
                      
                     
                      <Waktu data={this.state.dataBulan} />
                     
                     
                            <ul className="list-inline text-right">
                                <li>
                                    <h5><i className="fa fa-circle m-r-5 text-info"></i>Total Nasabah</h5> </li>
                                
                            </ul>
                            
                        </div>
                    </div>
                    
                </div>

               </div>
               {/* <footer className="footer text-center"> 2019 &copy; Sampun </footer> */}
        </div>
            </Fragment>
        )
    }
}

export default Dashboard;