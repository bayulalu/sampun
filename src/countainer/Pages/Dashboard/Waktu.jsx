import React ,{ Component, Fragment } from 'react';
import Char from '../../../component/Char/Char';
// import './Dasboard.css';
import { DatePicker, message } from "antd";
import 'antd/dist/antd.css';
import moment from 'moment';
// import Waktu from './Waktu';

const { RangePicker } = DatePicker;

const dateFormat = 'DD/mm/YYYY';



class Waktu extends Component{
	state = {
		mode : ['month', 'month'],
		mode2 : ['year', 'year'],
		dateRange : [],
		value : []
	}

	hendleCange = (event, date) => {
		console.log('tes')

	}
	hendleCange2 = (event, date) => {
		console.log('tes')

	}

  handlePanelChange = (value, mode) => {
    this.setState({
      value,
      mode: [
        mode[0] === 'date' ? 'month' : mode[0],
        mode[1] === 'date' ? 'month' : mode[1],
      ],
    });
  }

  handleChange = (value, date) => {
    console.log('kampret')
  }
  handleClick = () => {
    console.log(this.state.value)
  }

    render(){


	     if (this.props.data == 'Hari') {
        return (
            <Fragment>
            <RangePicker
            placeholder={['Awal', ' Sampai']}
              format={dateFormat}
           onChange={this.hendleCange}
            />
            <Char />
            </Fragment>

        )
    }else if(this.props.data== 'Bulan'){
      const { value, mode  } = this.state;
    
        return (
            <Fragment>
              <RangePicker
                  onChange={this.handleChange}
                    format='MM-YYYY'
                     value = {value}
                    onPanelChange={this.handlePanelChange}
                    placeholder={['Awal', ' Sampai']}      
                       mode = {mode}
              />
              <button  onClick={this.handleClick} className="btn btn-info"  >Submit</button>
              <Char  />
              </Fragment>
        )
    }else if(this.props.data == 'Tahun'){
        return (

            <Fragment>
              
                 <RangePicker
                  onChange={this.handleChange}
            placeholder={['Awal', ' Sampai']}
              format='MM-YYYY'
           onChange={this.hendleCange}
           mode= {this.state.mode2}
            />
                  <Char   />
                  </Fragment>
            )
    }else{
        return null
    }
   
    }
} 

export default Waktu;