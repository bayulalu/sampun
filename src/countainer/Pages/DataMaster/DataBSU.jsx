import React, {Component, Fragment} from 'react';
import Tes from '../../../component/Tes/Tes';

import {APIBSU} from '../../../service';
import swal from 'sweetalert';

class DataBSU extends Component{

    state = {
        bsu : [],
        postBsu : {
            id: '',
            alamat: '',
            kode_bsu : '',
            lat: '',
            lng: '',
            nama : '',
            nama_director : '',
            no_hp : '',
            email : ''
        },
        isUpdate: false
    }

     postApi = () => {
         
        const bsu = {
          nama: this.state.postBsu.nama,
          kode_bsu: this.state.postBsu.kode_bsu,
          alamat: this.state.postBsu.alamat,
          lat: this.state.postBsu.lat,
          lng: this.state.postBsu.lng,
          nama_director : this.state.postBsu.nama_director,
          no_hp : this.state.postBsu.no_hp,
          email: this.state.postBsu.email
        };

        APIBSU.postBSU(bsu).then((res) => {
            console.log(res);
            
            this.getBSU()
        })
                
     }

    getBSU = () => {
        APIBSU.getBSU().then(res => {
            this.setState({
                bsu : res
            })
        })

    }

    updateApi = () => {
        console.log(Number(this.state.postBsu.lat));
        const bsu = {
          id_data: this.state.postBsu.id,
          nama: this.state.postBsu.nama,
          kode_bsu: this.state.postBsu.kode_bsu,
          alamat: this.state.postBsu.alamat,
          lat: Number(this.state.postBsu.lat),
          lng: Number(this.state.postBsu.lng),
          nama_director : this.state.postBsu.nama_director,
          no_hp : this.state.postBsu.no_hp,
          email: this.state.postBsu.email
        };

        APIBSU.putBSU(bsu).then((res) => {
            this.getBSU()
            this.tambah()
        })
        
    }

    componentDidMount(){
        this.getBSU()
    }


    hendleRemove = (data) => {
        APIBSU.deleteBSU(data).then((res) => {
            this.getBSU()
        })
    }


    hendeleChange = (event) => {
        let postBsuNew = {...this.state.postBsu};

    
        postBsuNew[event.target.name] = event.target.value

        this.setState({
            postBsu: postBsuNew
        })
    }

    hendleSubmit = () => {
        const bsu = this.state.postBsu;
      
        
        if (bsu.alamat.trim() == '' || bsu.email.trim() == '' || bsu.kode_bsu.trim() == '' || bsu.lat.trim() == '' || bsu.lng.trim() == '' 
            || bsu.nama.trim() == '' || bsu.nama_director.trim() == '' || bsu.no_hp.trim() == '' ) {
                swal({
                    title: "Ops..!",
                    text: "Pastikan Anda Telah Mengisi Semua Data!!!",
                    icon: "error",
                  });
        }else{
            this.postApi()
        }
        
    }

    hendeleRestore = (data) => {
        //   swal("Berhasil!!", "Data Telah DiKembalikan", "success");
        APIBSU.restoreBSU(data).then((res) => {
            this.getBSU()
        })
        
    }

    hendleDeletePer = (data) => {
        APIBSU.permanenBSU(data).then((res) => {
            this.getBSU()
        })
    }

    hendeleUpdate = (data) => {
        // console.log(data)
        this.setState({
            postBsu: data,
            isUpdate : true
        }, () => {
        })
    }

    updateSubmit = () => {
        const bsu = this.state.postBsu;
        console.log(bsu);
        if (bsu.alamat == '' || bsu.email == '' || bsu.kode_bsu == '' || bsu.lat == '' || bsu.lng == '' 
        || bsu.nama == '' || bsu.nama_director == '' || bsu.no_hp == '') {
            swal({
                title: "Ops..!",
                text: "Pastikan Anda Telah Mengisi Semua Data!!!",
                icon: "error",
              });
        }else{
            this.updateApi()
            this.getBSU()

        }
       
    }

    tambah = ()=> {
        this.setState({
            postBsu: {
                id: '',
                alamat: '',
                kode_bsu : '',
                nama : '',
                lat: '',
                lng: '',
                nama_director : '',
                no_hp : '',
                email : ''
            },
            isUpdate : false
        }, () => {
        })
    }


	render(){
        let i = 0;
        let j = 0;
		return (
			<Fragment>
                
             
			<div id="page-wrapper">
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h2 className="page-title" id="tulis">Data BSU</h2> 
                    </div>
                </div>
                </div>

                <div className="container">
                <div className="row">
                <div className="col-sm-11">
                                
                <div className="white-box">
                            <div className="card-body">
                                <h4 className="card-title">Data BSU</h4>
                                
                                <div className="panel-group" id="accordion">
                                    <div className="panel panel-default">
                                        <div className="panel-heading">
                                        <div className="row">
										    <div className="col-md-9">
										    	 <button type="button" className=" btn btn-primary btn-rounded m-t-10 float-right tambahData" data-toggle="modal" data-target="#add-contact" onClick={this.tambah}>Tambah Data</button>


								<div id="add-contact" className="modal fade in" role="dialog" aria-labelledby="mymodallabel" aria-hidden="true">
                                    <div className="modal-dialog">
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 className="modal-title" id="mymodallabel">Tambah Data BSU</h4> </div>
                                            <div className="modal-body">
                                                <form className="form-horizontal form-material" >
                                                    <div className="form-group">
                                                       <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control" onChange={this.hendeleChange} value={this.state.postBsu.kode_bsu} name="kode_bsu" placeholder="kode" /> </div>
                                                        
                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control" onChange={this.hendeleChange} name="nama" value={this.state.postBsu.nama} placeholder="nama" /> </div>
                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control" onChange={this.hendeleChange} name="alamat" value={this.state.postBsu.alamat} placeholder="alamat" /> 
                                                        </div>

                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control" onChange={this.hendeleChange} name="lat" value={this.state.postBsu.lat} placeholder="lat" /> 
                                                        </div>

                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control" onChange={this.hendeleChange} name="lng" value={this.state.postBsu.lng} placeholder="lng" /> 
                                                        </div>
                                                                                                     
                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control" onChange={this.hendeleChange} name="nama_director" value={this.state.postBsu.nama_director} placeholder="Nama Director" /> 
                                                        </div>

                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control" onChange={this.hendeleChange} name="no_hp" value={this.state.postBsu.no_hp} placeholder="No HP / Wa" /> 
                                                        </div>

                                                        <div className="col-md-12 m-b-20">
                                                            <input type="email" className="form-control" onChange={this.hendeleChange} name="email" value={this.state.postBsu.email} placeholder="Email" /> 
                                                        </div>
                                                        
                                                        
                                                    </div>
                                                    <div className="modal-footer">
                                                <button type="button" className="btn btn-info waves-effect" onClick={this.hendleSubmit} data-dismiss="modal">Simpan</button>
                                                <button type="button" className="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                                            </div>
                                                </form>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>



                                <div id="add-update" className="modal fade in" role="dialog" aria-labelledby="mymodallabel" aria-hidden="true">
                                    <div className="modal-dialog">
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 className="modal-title" id="mymodallabel"> Update Data</h4> </div>
                                            <div className="modal-body">
                                                <form className="form-horizontal form-material" method="post" action='https://sampun.alphacstudios.com/api/data/bsu/update' >
                                                    <div className="form-group">
                                                        <input type="hidden" value={this.state.postBsu.id} name="id" />
                                                       <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control" onChange={this.hendeleChange} value={this.state.postBsu.kode_bsu} name="kode_bsu" placeholder="kode" /> </div>
                                                        
                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control" onChange={this.hendeleChange} name="nama" value={this.state.postBsu.nama} placeholder="nama" /> </div>
                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control" onChange={this.hendeleChange} name="alamat" value={this.state.postBsu.alamat} placeholder="alamat" /> </div>
                                                    <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control" onChange={this.hendeleChange} name="lat" value={this.state.postBsu.lat} placeholder="lat" /> 
                                                        </div>

                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control" onChange={this.hendeleChange} name="lng" value={this.state.postBsu.lng} placeholder="lng" /> 
                                                        </div>
                                                        
                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control" onChange={this.hendeleChange} name="nama_director" value={this.state.postBsu.nama_director} placeholder="Nama Director" /> 
                                                        </div>

                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control" onChange={this.hendeleChange} name="no_hp" value={this.state.postBsu.no_hp} placeholder="No HP / Wa" /> 
                                                        </div>

                                                        <div className="col-md-12 m-b-20">
                                                            <input type="email" className="form-control" onChange={this.hendeleChange} name="email" value={this.state.postBsu.email} placeholder="Email" /> 
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                </form>
                                            </div>
                                            <div className="modal-footer">
                                                <button type="button" className="btn btn-info waves-effect" onClick={this.updateSubmit} data-dismiss="modal">Simpan</button>
                                                <button type="button" className="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

										    </div>

										    <div className="offset-md-9 col-md-3 ">
										    <br />
										    	<input type="searech" id="cari" placeholder="Pencarian" />
										    </div>

                                        </div>
                                        </div>
                                        <div id="collapseOne" className="panel-collapse collapse in">

                                            <div className="panel-body">
                                                <div className="card-body">
                                                    <div className="table-responsive">
                                                        <table id="demo-foo-accordion" className="table table-bordered m-b-0 toggle-arrow-tiny" data-filtering="true" data-paging="true" data-sorting="true">
                                                            <thead>
                                                                <tr className="footable-filtering">
                                                                	<th>No</th> 
                                                                	<th>Kode</th>
                                                                    <th data-toggle="true"> Nama </th>
                                                                    <th data-hide="phone"> Alamat </th>
                                                                    <th data-hide="all"> Total Member </th>
                                                                    <th>lat</th>
                                                                    <th>lng</th>
                                                                    <th>Nama Director</th>
                                                                    <th>No Hp /WA</th>
                                                                    <th>Email</th>
                                                                     <th  > Edit </th>
                                                                    <th > Hapus </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            
                                                                {
                                                                   Object.keys(this.state.bsu).map((key, index) => {
                                                                 
                                                                       if (this.state.bsu[key].status == 1) {

                                                                            return <Tes 
                                                                            key={index} kode={this.state.bsu[key].kode_bsu}
                                                                             nama={this.state.bsu[key].nama } 
                                                                             
                                                                            alamat={this.state.bsu[key].alamat} total={this.state.bsu[key].total_member}
                                                                            status={this.state.bsu[key].status} romove={this.hendleRemove} 
                                                                            update={this.hendeleUpdate}  lat={this.state.bsu[key].lat} 
                                                                            lng={this.state.bsu[key].lng} id={key} no={j = j + 1} 
                                                                            nama_director ={this.state.bsu[key].nama_director} email={this.state.bsu[key].email}
                                                                           no_hp ={this.state.bsu[key].no_hp}
                                                                             />
                                                                       }
                                                                      
                                                                        
                                                                  })    
                                                                }
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                                                                
                </div>
                </div>


                                                                
                <div className="row">
                <div className="col-sm-11">
                          <div className="white-box">
                            <div className="card-body">
                                <h4 className="card-title">Data BSU yang di hapus</h4>
                                
                                <div className="panel-group" id="accordion">
                                    <div className="panel panel-default">
                                        <div className="panel-heading">
                                        <div className="row">
										    
										    <div className="col-md-9">
										    	
										    </div>

										    <div className="col col-md-3 offset-md-9">
										    	<input type="searech" id="cari" placeholder="Pencarian" />
										    </div>

                                        </div>
                                        </div>
                                        <div id="collapseOne" className="panel-collapse collapse in">

                                            <div className="panel-body">
                                                <div className="card-body">
                                                    <div className="table-responsive">
                                                        <table id="demo-foo-accordion" className="table table-bordered m-b-0 toggle-arrow-tiny" data-filtering="true" data-paging="true" data-sorting="true">
                                                            <thead>
                                                                <tr className="footable-filtering">
                                                                <th>No</th> 
                                                                	<th>Kode</th>
                                                                    <th data-toggle="true"> Nama </th>
                                                                    <th data-hide="phone"> Alamat </th>
                                                                    <th data-hide="all"> Total Member </th>
                                                                    <th>lat</th>
                                                                    <th>lng</th>
                                                                    <th>Nama Director</th>
                                                                    <th>No Hp /WA</th>
                                                                    <th>Email</th>
                                                                   <th > Kembalikan </th>
                                                                    <th > Hapus </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                               {
                                                                   Object.keys(this.state.bsu).map((key, index) => {
                                                                       if (this.state.bsu[key].status == 0) {
                                                                        
                                                                            return <Tes key={index} 
                                                                                    kode={this.state.bsu[key].kode_bsu} nama={this.state.bsu[key].nama } 
                                                                                    alamat={this.state.bsu[key].alamat} total={this.state.bsu[key].total_member}
                                                                                    status={this.state.bsu[key].status}  restore={this.hendeleRestore} id={key} 
                                                                                    deletee={this.hendleDeletePer} lat={this.state.bsu[key].lat} 
                                                                                    lng={this.state.bsu[key].lng}  no={i = i + 1} 
                                                                                    nama_director ={this.state.bsu[key].nama_director} email={this.state.bsu[key].email}
                                                                                    no_hp ={this.state.bsu[key].no_hp}
                                                                                    />

                                                                       }    
                                                                  })    
                                                                }
                                                                
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                    </div> 
                </div>
			</Fragment>
		)
	}
}

export default DataBSU;