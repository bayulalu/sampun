import React, {Component, Fragment} from 'react';

import {SAMPAH} from '../../../service';
import swal from 'sweetalert';

let Tes = (props) => {

   let data = {
        id: props.id,
        kode : props.kode,
        harga : props.harga,
        jenis_sampah: props.jenis_sampah,
        satuan: props.satuan
   }
   if (props.status == 1) {

        return (
             <tr>
                <td>{props.no}</td>
                <td>{props.kode}</td>
                <td>{props.jenis_sampah}</td>
                <td>{props.satuan}</td>
                <td>{props.harga}</td>
                 <td>{props.kategori}</td>
                <td>
                    <button type="button" className="btn-sm btn-info" data-toggle="modal" data-target="#add-update" 
                     onClick={() => props.update(data)}><i className="mdi mdi-settings"> </i></button>  </td>
                      <td>
                    <button type="button" className="btn-sm  btn-warning" onClick={() => props.romove(props.id)} ><i className="mdi mdi-delete"></i> </button> </td>
            </tr>
        )
   }else{
        return (
             <tr>
                 <td>{props.no}</td>
                <td>{props.kode}</td>
                <td>{props.nama}</td>
                <td>{props.alamat}</td>
                <td>{props.total}</td>
                <td>{props.kategori}</td>
                <td>
                    <button type="button" className="btn-sm btn-success"  onClick={() => props.restore(props.id)} ><i className="mdi mdi-update"> </i></button>  </td>
                     <td>
                    <button type="button" className="btn-sm  btn-danger" onClick={() => props.deletee(props.id)} ><i className="mdi mdi-delete-forever"></i></button> </td>
            </tr>
        )
   }
   
}


class Sampah extends Component{
    state = {
        sampah : [],
        postSampah: {
            id: '',
            kode: '',
            satuan: '',
            jenis_sampah : '',
            harga : '',
            kategori: ''
        },
        isUpdate : false
    }


    getsampah = () => {
       
        SAMPAH.sampah().then(res => {
            this.setState({
                sampah : res
            })
        })
    }

   
    updateApi = () => {
        const sampah = {
          id_data: this.state.postSampah.id,
          satuan: this.state.postSampah.satuan,
          kode: this.state.postSampah.kode,
          jenis_sampah: this.state.postSampah.jenis_sampah,
          harga: this.state.postSampah.harga,
          kategori: this.state.postSampah.kategori
        };
        
        SAMPAH.putSampah(sampah).then(res => {
            this.getsampah()
        })
        
     }


    postApi = () => {
        const sampah = {
          kode: this.state.postSampah.kode,
          satuan: this.state.postSampah.satuan,
          jenis_sampah: this.state.postSampah.jenis_sampah,
          harga: this.state.postSampah.harga,
          kategori : this.state.postSampah.kategori
        };

        console.log(sampah);
        

        SAMPAH.postSampah(sampah).then((res) => {
            this.getsampah()
        })
        
     }
     hendleSubmit = () => {
         const sampah = this.state.postSampah;
        
         if (sampah.kode.trim() == '' || sampah.satuan.trim() == '' || sampah.jenis_sampah.trim() == '' 
            || sampah.harga.trim() == '' || sampah.kategori.trim() == '') {
                swal({
                    title: "Ops..!",
                    text: "Pastikan Anda Telah Mengisi Semua Data!!!",
                    icon: "error",
                  });
         }else{
            this.postApi()
            this.getsampah()
         }
       
    }

    hendeleChange = (event) => {
        let postSampahNew = {...this.state.postSampah};

    
        postSampahNew[event.target.name] = event.target.value

        this.setState({
            postSampah: postSampahNew
        }, () => {
        })
    }

    componentDidMount(){
        this.getsampah()
    }

    hendleRemove = (data) => {
        SAMPAH.deleteSampah(data).then((res) => {
            
        })
        this.getsampah()
    }

    updateSubmit = () => {
        const sampah = this.state.postSampah;
       if (sampah.kode == '' || sampah.satuan == '' || sampah.jenis_sampah == '' 
       || sampah.harga == '' || sampah.kategori == '') {
            swal({
                title: "Ops..!",
                text: "Pastikan Anda Telah Mengisi Semua Data!!!",
                icon: "error",
            });
       }else{
            this.updateApi()
            this.getsampah()
       }
       
    }

    hendeleRestore = (data) => {
        SAMPAH.restoreSampah(data).then((res) => {
            this.getsampah()
        })
    }

    hendleDeletePer = (data) => {
        SAMPAH.permanenSampah(data).then((res) => {
            this.getsampah()
        })
        
    }

    hendeleUpdate = (data) => {
    this.setState({
        postSampah: data,
        isUpdate : true
    }, () => {
        console.log(this.state.isUpdate)
    })
    }

    tambah = ()=> {
        this.setState({
            postSampah: {
                id: '',
                kode: '',
                jenis_sampah : '',
                satuan: '',
                harga : '',
                kategori: ''
            },
            isUpdate : false
        }, () => {
            console.log(this.state.isUpdate)
        })
    }

	render(){
        let i = 0;
        let j = 0;
		return (
			<Fragment>           
			<div id="page-wrapper">
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h2 className="page-title" id="tulis">Data Sampah</h2> 
                    </div>
                </div>
                </div>

                <div className="container">
                <div className="row">
                <div className="col-sm-11">

                
                <div className="white-box">
                            <div className="card-body">
                                <h4 className="card-title">Data Sampah</h4>
                                
                                <div className="panel-group" id="accordion">
                                    <div className="panel panel-default">
                                        <div className="panel-heading">
                                        <div className="row">
										    <div className="col-md-9">
										    	 <button type="button" className="tambahData btn btn-primary btn-rounded m-t-10 float-right" data-toggle="modal" data-target="#add-contact" onClick={this.tambah}>Tambah Data</button>


							<div id="add-contact" className="modal fade in" role="dialog" aria-labelledby="mymodallabel" aria-hidden="true">
                                    <div className="modal-dialog">
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 className="modal-title" id="mymodallabel"> {this.state.isUpdate ? 'Update' : 'Tambah'}  Data</h4> </div>
                                            <div className="modal-body">
                                                <form className="form-horizontal form-material" method="post" action='https://sampun.alphacstudios.com/api/data/sampah/add' >
                                                    <div className="form-group">
                                                        <input type="hidden" value={this.state.postSampah.id} name="id_data" />
                                                       <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control"  onChange={this.hendeleChange} value={this.state.postSampah.kode}  name="kode" placeholder="kode" /> </div>
                                                        
                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control"  onChange={this.hendeleChange}  name="jenis_sampah" value={this.state.postSampah.jenis_sampah} placeholder="Jenis Sampah" /> </div>
                                                        
                                                           <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control"  onChange={this.hendeleChange}  name="satuan" value={this.state.postSampah.satuan} placeholder="Satuan" /> </div>
                                                         
                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control"  onChange={this.hendeleChange}  name="harga" value={this.state.postSampah.harga} placeholder="Harga" /> </div>

                                                            <div className="col-md-12 m-b-20">
                                                                <label >Katagori</label>
                                                                <select className="form-control" onChange={this.hendeleChange} name='kategori' value={this.state.postSampah.kategori}>
                                                                <option value=''>Pilih</option>
                                                                <option value='palstik'>Plastik</option>
                                                                <option value='kertas'>Kertas</option>
                                                                <option value='kaca'>Kaca</option>
                                                                <option value='logam' >Logam</option>
                                                                </select>
                                                            </div>

                                                    </div>
                                                    
                                                    
                                                </form>
                                            </div>
                                           <div className="modal-footer">
                                                <button type="button" className="btn btn-info waves-effect" onClick={this.hendleSubmit} data-dismiss="modal">Simpan</button>
                                                <button type="button" className="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                               

                                <div id="add-update" className="modal fade in" role="dialog" aria-labelledby="mymodallabel" aria-hidden="true">
                                    <div className="modal-dialog">
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 className="modal-title" id="mymodallabel"> {this.state.isUpdate ? 'Update' : 'Tambah'}  Data</h4> </div>
                                            <div className="modal-body">
                                                <form className="form-horizontal form-material" method="post" action="https://sampun.alphacstudios.com/api/data/sampah/update">
                                                    <div className="form-group">
                                                        <input type="hidden" value={this.state.postSampah.id} name="id_data" />
                                                       <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control"  onChange={this.hendeleChange} value={this.state.postSampah.kode}  name="kode" placeholder="kode" /> </div>
                                                        
                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control"  onChange={this.hendeleChange}  name="jenis_sampah" value={this.state.postSampah.jenis_sampah} placeholder="Jenis Sampah" /> </div>
                                                        <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control"  onChange={this.hendeleChange}  name="satuan" value={this.state.postSampah.satuan} placeholder="Satuan" /> </div> 
                                                            <div className="col-md-12 m-b-20">
                                                            <input type="text" className="form-control"  onChange={this.hendeleChange}  name="harga" value={this.state.postSampah.harga} placeholder="Harga" /> </div>                                                        
                                                           
                                                            <div className="col-md-12 m-b-20">
                                                                <label >Katagori</label>
                                                                <select className="form-control" onChange={this.hendeleChange} name='kategori' value={this.state.postSampah.kategori}>
                                                                <option value=''>Pilih</option>
                                                                <option value='palstik'>Plastik</option>
                                                                <option value='kertas'>Kertas</option>
                                                                <option value='kaca'>Kaca</option>
                                                                <option value='logam' >Logam</option>
                                                                </select>
                                                            </div>
                                                    </div>
                                                    
                                                </form>
                                            </div>
                                           <div className="modal-footer">
                                                <button type="button" className="btn btn-info waves-effect" onClick={this.updateSubmit} data-dismiss="modal">Simpan</button>
                                                <button type="button" className="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
										    </div>

										    
                                            <div className="offset-md-9 col-md-3 ">
                                            <br />
                                                <input type="searech" id="cari" placeholder="Pencarian" />
                                            </div>

                                        </div>
                                        </div>
                                        <div id="collapseOne" className="panel-collapse collapse in">

                                            <div className="panel-body">
                                                <div className="card-body">
                                                    <div className="table-responsive">
                                                        <table id="demo-foo-accordion" className="table table-bordered m-b-0 toggle-arrow-tiny " data-filtering="true" data-paging="true" data-sorting="true">
                                                            <thead>
                                                                <tr className="footable-filtering">
                                                                	<th>No</th> 
                                                                	<th>Kode</th>
                                                                    <th data-toggle="true"> Jenis Sampah </th>
                                                                    <th data-hide="phone"> Satuan / kg</th>
                                                                    <th data-hide="all"> Harga </th> 
                                                                   
                                                                    <th>kategori</th>
                                                                    <th  > Edit </th>
                                                                    <th > Hapus </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                               {
                                                                Object.keys(this.state.sampah).map((key, index) => {
                                                             
                                                                   if (this.state.sampah[key].status == 1) {

                                                                        return <Tes 
                                                                            key={index} kode={this.state.sampah[key].kode}
                                                                            jenis_sampah={this.state.sampah[key].jenis_sampah } 
                                                                            satuan={this.state.sampah[key].satuan} harga={this.state.sampah[key].harga}  
                                                                            status={this.state.sampah[key].status} romove={this.hendleRemove} 
                                                                            update={this.hendeleUpdate} id={key} no={i = i + 1} 
                                                                            kategori={this.state.sampah[key].kategori}
                                                                            /> 
                                                                   }
                                                                  
                                                                    
                                                              })    
                                                            }
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        </div>
                        </div>



                        <div className="row">
                        <div className="col-sm-11">
                          <div className="white-box">
                            <div className="card-body">
                                <h4 className="card-title">Data Sampah yang di hapus</h4>
                                
                                <div className="panel-group" id="accordion">
                                    <div className="panel panel-default">
                                        <div className="panel-heading">
                                        <div className="row">
										    
										    <div className="col-md-9">
										    	
										    </div>

										    <div className="col col-md-3 offset-md-9">
										    	<input type="searech" id="cari" placeholder="Pencarian" />
										    </div>

                                        </div>
                                        </div>
                                        <div id="collapseOne" className="panel-collapse collapse in">

                                            <div className="panel-body">
                                                <div className="card-body">
                                                    <div className="table-responsive">
                                                        <table id="demo-foo-accordion" className="table table-bordered m-b-0 toggle-arrow-tiny" data-filtering="true" data-paging="true" data-sorting="true">
                                                            <thead>
                                                                <tr className="footable-filtering">
                                                                	<th>No</th> 
                                                                    <th>Kode</th>
                                                                    <th data-toggle="true"> Jenis Sampah </th>
                                                                    <th data-hide="phone"> Satuan / kg</th>
                                                                    <th data-hide="all"> Harga </th> 
                                                                    <th>kategori</th>
                                                                    <th > Kembalikan </th>
                                                                    <th > Hapus </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                {
                                                                   Object.keys(this.state.sampah).map((key, index) => {
                                                                       if (this.state.sampah[key].status == 0) {
                                                                               return <Tes key={index} 
                                                                                    kode={this.state.sampah[key].kode} nama={this.state.sampah[key].jenis_sampah } 
                                                                                    alamat={this.state.sampah[key].satuan} total={this.state.sampah[key].harga}
                                                                                    status={this.state.sampah[key].status} restore={this.hendeleRestore} id={key} 
                                                                                    deletee={this.hendleDeletePer} no={j = j + 1}  kategori={this.state.sampah[key].kategori} />

                                                                       }    
                                                                  })    
                                                                }
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            </div>
                            </div>
                        </div>


                        </div>
                </div>
			</Fragment>
		)
	}
}

export default Sampah;