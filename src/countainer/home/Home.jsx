import React, {Component, Fragment} from 'react';
import {BrowserRouter as Router , Route, Switch} from 'react-router-dom';
import Loadable from 'react-loadable';

// pages
import Navbar from './Navbar';
import Slidebar from './SlideBar';


import AuthContextProvider from '../../context/AuthContextProvider';
import Login from '../pages/login/Login';

import ProtectedRoute from '../../component/login/ProtectedRoute';

function Loading() {
    return <h3>Sampun Loding julu</h3>;
  }



  const Tamplate = () => {
    return(
      <Fragment>
      <Navbar /> 
      <Slidebar />
      </Fragment>
    )
  }


  const DataBSI = Loadable({
    loader: () => import('../pages/dataMaster/DataBSI'),
    loading: Loading,
    delay: 60
  });
  
  const DataReport = Loadable({
    loader: () => import('../pages/report/Report'),
    loading: Loading,
    delay: 60
  });

  const DataNasabah = Loadable({
    loader: () => import('../pages/memberManajer/Nasabah'),
    loading: Loading,
    delay: 60
  });

  const DataBSU = Loadable({
    loader: () => import('../pages/dataMaster/DataBSU'),
    loading: Loading,
    delay: 60
  });
  

  const DataSampah = Loadable({
    loader: () => import('../pages/dataMaster/Sampah'),
    loading: Loading,
    delay: 60
  });


  const DataPetugas = Loadable({
    
    loader: () => import('../pages/petugasManajer/DataPetugas'),
    loading: Loading,
    delay: 60
  });


  

  
  const TambahAdmin = Loadable({
    loader: () => import('../pages/userManajer/Tambah'),
    loading: Loading,
    delay: 60
  })

  const Dashboard = Loadable({
    loader: () => import('../pages/dashboard/Dashboard'),
    loading: Loading,
    delay: 60
  })



 
  let NoMatch = () => {
    return <h1>Tidak di temuakan</h1>
  }

  
class Home extends Component{
    state = {
        showComponent: false
      };
    render(){
        return(

            <Fragment>
              
            <AuthContextProvider>

            <Router>
           
              {

                  localStorage.getItem('user') == null ? null : <Tamplate /> 
                }
                <Switch>               
                <ProtectedRoute path='/' exact component={Login}  />
                
                <ProtectedRoute path='/login' exact component={Login}  />

                <ProtectedRoute path='/dashboard'  component={Dashboard}  />
                <ProtectedRoute path="/nasabah"  component={DataNasabah} />
                <ProtectedRoute path="/Report" component={DataReport} />  
                <ProtectedRoute path='/data-bsi' component={DataBSI} />
                <ProtectedRoute path='/data-bsu' component={DataBSU} />
                <ProtectedRoute path='/sampah' component={DataSampah} />
                <ProtectedRoute path='/data-petugas' component={DataPetugas} />
                <ProtectedRoute path='/tambah-admin' component={TambahAdmin} />
                <Route component={NoMatch} /> 
                </Switch>

            </Router>
            </AuthContextProvider>
            </Fragment>
        )
    }

}

export default Home; 