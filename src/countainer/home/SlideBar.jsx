import React , { Component, Fragment}from 'react';

import {BrowserRouter as Router , Route, Link} from 'react-router-dom';
import './SlideBar.css';
import Logout from '../../component/logout/logout';


class SlideBar extends Component{
    state = {
        user: []
    }

    componentDidMount(){
       let datas = JSON.parse(localStorage.getItem('user'));
              
       
       this.setState({
        user : datas
       })

       
    }

    render(){
        return(
            <Fragment>
                <div className="navbar-default sidebar" role="navigation">
        <a href="/dashboard" className="waves-effect"> <span id="logo-slide"><img height="50" src="plugins/images/sampun.png" /> </span>
                        </a>
           
            <div className="sidebar-nav slimscrollsidebar">
                <ul className="nav" id="side-menu">
                    <li className="user-pro">
                        <a href="/" className="waves-effect"><img src="plugins/images/users/1.jpg" alt="user-img" className="img-circle" /> <span className="hide-menu"> {this.state.user.nama}<span className="fa arrow"></span></span>
                        </a>
                        <ul className="nav nav-second-level collapse " aria-expanded="false" >
                            <li><a href="#"><i className="ti-user"></i> <span className="hide-menu">My Profile</span></a></li>
                            <li><a href="#"><i className="ti-wallet"></i> <span className="hide-menu">My Balance</span></a></li>
                            <li><a href="#"><i className="ti-settings"></i> <span className="hide-menu">Account Setting</span></a></li>
                            <Logout />
                           </ul>
                    </li>

                     <li> <Link to='/dashboard'><i data-icon="&#xe026;" className="linea-icon linea-basic fa-fw"></i> <span className="hide-menu">Dashboard</span></Link> 
                               
                    </li>

                    <li> <a href="#" className="waves-effect"><i className="mdi mdi-format-color-fill fa-fw"></i> <span className="hide-menu">Master Data<span className="fa arrow"></span> </span></a>
                        <ul className="nav nav-second-level">
                            <li><Link to='/data-bsi'><i data-icon="&#xe026;" className="linea-icon linea-basic fa-fw"></i> <span className="hide-menu">Data BSI</span></Link></li>
                            <li><Link to='/data-bsu'><i data-icon="&#xe025;" className="linea-icon linea-basic fa-fw"></i> <span className="hide-menu">Data BSU</span></Link></li>
                            <li><Link to='/sampah'><i data-icon="&#xe025;" className="linea-icon linea-basic fa-fw"></i> <span className="hide-menu">Data Sampah</span></Link></li>
                         </ul>
                    </li>

                    <li> <a href="javascript:void(0);" className="waves-effect"><i className="icon icon-user " data-icon="v"></i> <span className="hide-menu"> Nasabah Manajer  <span className="fa arrow"></span> </span></a>
                        <ul className="nav nav-second-level ">
                            <li>  <Link to='/nasabah' > <i className="icon  icon-user-follow" data-icon="v"></i> <span className="hide-menu">Data Nasabah </span></Link> </li>
                            <li> <Link to='/report'> <i className="icon  icon-printer " data-icon="v"></i> <span className="hide-menu">Nasabah Report </span></Link> </li>
                        </ul>
                    </li>

                    <li><a href="inbox.html" className="waves-effect"><i className="mdi mdi-account-check "></i> <span className="hide-menu">Petugas Manajer<span className="fa arrow"></span></span></a>
                        <ul className="nav nav-second-level">
                            <li><Link to='/data-petugas'><i className="mdi mdi-account-check"></i><span className="hide-menu">Data Petugas</span></Link></li>
                           
                        </ul>
                    </li>


                    <li><a href="inbox.html" className="waves-effect"><i className="mdi mdi-account-check "></i> <span className="hide-menu">User Manajer<span className="fa arrow"></span></span></a>
                    <ul className="nav nav-second-level">
                        <li><Link to='/tambah-admin'><i className="mdi mdi-account-check"></i><span className="hide-menu">Tambah Admin</span></Link></li>
                     
                    </ul>
                </li>
                   
                   <Logout />
                    </ul>
            </div>
        </div>
        
            </Fragment>
        )
    }
}

export default SlideBar;