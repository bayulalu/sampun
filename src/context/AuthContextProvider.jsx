import React, {Component, Fragment} from 'react';
import axios from 'axios';
import swal from 'sweetalert';



const AuthContext = React.createContext()

class AuthContextProvider extends Component{
    constructor(){
        super()
        this.state = {
            users: [],
            user: localStorage.getItem('user') || "",
            isLoggedIn: (localStorage.getItem('user') === null) ? false : true
        }
    }


    // login 
    login = (credentials) => {
        console.log(credentials);
        return axios.post('http://35.198.225.173/api/login', credentials)
                    .then(res => {

                       let user = res.data
                       console.log(res.data)
                        localStorage.setItem("user", JSON.stringify(res.data))

                        this.setState({
                            user,
                            isLoggedIn: true
                        })

                        // return console.log(res)
                        window.location.href = "/dashboard  "
                    }, (err) => {
                        swal({
                            title: "Ops..!",
                            text: "Cek No Hp Dan Kata Sandi Anda!!",
                            icon: "error",
                          });
                       
                    })
    }

    logout = () => {
        localStorage.removeItem("user")

        this.setState({
            // token,
            isLoggedIn: false
        })

        // return console.log('logout')
        window.location.href = "/"
    }

    // initUser = () => {
    //     return axios.get('http://localhost:8000/api/profile')
    //                 .then(res => {
    //                     console.log(res)
    //                     this.setState({user: res.data})
    //                     return res;
    //                 })
    // }

    render(){
        return(
            <AuthContext.Provider value= {{
                                login: this.login,
                                logout: this.logout,
                                ...this.state
                                }}> 
                {this.props.children}
            </AuthContext.Provider>
        )
    }
}


export default AuthContextProvider;

// HOX
export const withAuth = (WrappedComponent) => {
    return class extends Component {
        render() {
            return (
                <AuthContext.Consumer>
                    {(context) => (
                        <Fragment>
                        <WrappedComponent {...this.props} {...context} />
                        </Fragment>
                    )}
                </AuthContext.Consumer>
            )
        }
    }
}

 