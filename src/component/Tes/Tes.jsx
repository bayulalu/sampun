import React , {Component, Fragment} from 'react';

let Tes = (props) => {
   let data = {
        id: props.id,
        kode_bsu : props.kode,
        alamat : props.alamat,
        nama: props.nama,
        lat: props.lat,
        lng: props.lng,
        nama_director : props.nama_director,
        no_hp : props.no_hp,
        email : props.email
   }

  
   if (props.status == 1) {

        return (
             <tr>
                 <td> {props.no} </td>
                <td>{props.kode}</td>
                <td>{props.nama}</td>
                <td>{props.alamat}</td>
                <td>{props.total}</td>
                <td>{props.lat}</td>
                <td>{props.lng}</td>
                <td>{props.nama_director}</td>
                <td>{props.no_hp}</td>
                <td>{props.email}</td>
                <td>
                    <button type="button" className="btn-sm btn-info" data-toggle="modal" data-target="#add-update" 
                     onClick={() => props.update(data)}><i className="mdi mdi-settings"> </i> </button> </td> 
                     <td>
                    <button type="button" className="btn-sm  btn-warning" onClick={() => props.romove(props.id)} ><i className="mdi mdi-delete"></i></button> </td>
            </tr>
        )
   }else{
        return (
             <tr>
              <td> {props.no} </td>
                <td>{props.kode}</td>
                <td>{props.nama}</td>
                <td>{props.alamat}</td>
                <td>{props.total}</td>
                 
                <td>{props.lat}</td>
                <td>{props.lng}</td>
                <td>{props.nama_director}</td>
                <td>{props.no_hp}</td>
                <td>{props.email}</td>
                <td>
                    <button type="button" className="btn-sm btn-success"  onClick={() => props.restore(props.id)} ><i className="mdi mdi-update"> </i> </button>  </td>
                    <td>
                    <button type="button" className="btn-sm  btn-danger" onClick={() => props.deletee(props.id)} ><i className="mdi mdi-delete-forever"></i></button> </td>
            </tr>
        )
   }
   
}

export default Tes;