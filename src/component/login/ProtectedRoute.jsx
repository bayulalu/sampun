import React from "react";

import { Route, Redirect } from 'react-router-dom';
import { withAuth } from "../../context/AuthContextProvider";
import Login from "../../countainer/pages/login/Login"; 


function ProtectedRoute(props) {
    const {component: Component, ...rest } = props

    return (
        props.isLoggedIn ? (<Route {...rest} component={Component} />)
                         : (  <Route to='/' component={Login}  />)

        // props.isLoggedIn ? <Route {...rest} component={Component} />
        // :  <Route push to='/' />
    )
}

export default withAuth(ProtectedRoute)