import React, {Component, Fragment} from 'react';
import { Bar,Doughnut } from 'react-chartjs-2';


let MyChart = () => {

  const data = {
    labels: ['Januari', 'Februari', 'Maret', 'April', 'Mai', 'Juni', 'Juli'],
    datasets: [
      {
        label: 'Grafik Pertumbuhan Nasabah',
        fill: true,
        lineTension: 0.1,
        backgroundColor: 'rgba(75,192,192,0.4)',
        borderColor: 'rgba(75,192,192,1)',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: 'rgba(75,192,192,1)',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'rgba(75,192,192,1)',
        pointHoverBorderColor: 'rgba(220,220,220,1)',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: [70, 100, 200, 300, 400, 500, 700]
      }
    ]
  };
  return  <Bar data={data} />
}
export default MyChart;