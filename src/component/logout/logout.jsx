import React from "react";

import { withAuth } from "../../context/AuthContextProvider"; 

let Logout = (props) => {
    return(
        <li><a href="/login" onClick={props.logout}><i className="fa fa-power-off"></i> <span className="hide-menu">Logout</span></a></li>
                        
    )
}

export default withAuth(Logout);
