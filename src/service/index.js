import Get from './Get';
import Post from './Post';
import Delete from './Delete';
// BSI
const getBSI = () => Get('api/data/bsi');
const postBSI = (data) => Post('api/data/bsi/add', data);
const deleteBSI = (data) =>  Delete(`api/data/bsi/delete/${data}`);
const restoreBSI = (data) => Get(`api/data/bsi/restore/${data}`);
const permanenBSI = (data) => Get(`api/data/bsi/deletep/${data}`);
const putBSI = (data) => Post(`api/data/bsi/update`, data);

export const APIBSI = {
    getBSI,
    postBSI,
    deleteBSI,
    restoreBSI,
    permanenBSI,
    putBSI
}


// BSU 
const getBSU = () => Get('api/data/bsu');
const postBSU = (data) => Post('api/data/bsu/add', data);
const deleteBSU = (data) =>  Delete(`api/data/bsu/delete/${data}`);
const restoreBSU = (data) => Get(`api/data/bsu/restore/${data}`);
const permanenBSU = (data) => Get(`api/data/bsu/deletep/${data}`);
const putBSU = (data) => Post('api/data/bsu/update', data);
export const APIBSU = {
    getBSU,
    postBSU,
    deleteBSU,
    restoreBSU,
    permanenBSU,
    putBSU
}


//DASHBORD 
const totalNasabah = () => Get('api/data/nasabah/total');
export const APIDAS = {
    totalNasabah
} 


// SAMPAH
const sampah = () => Get('api/data/sampah');
const postSampah = (data) => Post('api/data/sampah/add', data);
const deleteSampah = (data) =>  Delete(`api/data/sampah/delete/${data}`);
const restoreSampah = (data) => Get(`api/data/sampah/restore/${data}`);
const permanenSampah = (data) => Get(`api/data/sampah/deletep/${data}`);
const putSampah = (data) => Post(`api/data/sampah/update`, data);
export const SAMPAH = {
    sampah,
    postSampah,
    deleteSampah,
    restoreSampah,
    permanenSampah ,
    putSampah
}

// APPROVAL 



const approv = (data) => Post('api/approve/update/idbsu', data);
const aktif = (data) => Get(`api/data/petugas/approve/${data}`);

export const APIAPPROV = {
    approv,
    aktif
}


// Tambah Admin
const admin = () => Get('/api/user/login');
const addAdmin = (data) => Post(`api/profile/login`, data);
export const ADDADMIN = {
    admin,
    addAdmin
}


// Member / Nasabah
const nasabah = () => Get('api/data/nasabah');
const putNasabah = (data) => Post('api/data/nasabah/update', data)
const permanenNasabah = (data) => Get(`/api/data/nasabah/deletep/${data}`);
export const NASABAH = {
    nasabah,
    permanenNasabah,
    putNasabah
}

