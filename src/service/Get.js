import axios from 'axios';
import {RootPath} from './Config';

const Get = (root) => {
    const promise = new Promise((resolve, reject ) => {
        axios.get(`${RootPath}/${root}`)
            .then((res) => {
                resolve(res.data)
        }, (err) => {
            reject(err)
        })
    })
    return promise;
} 


export default Get;