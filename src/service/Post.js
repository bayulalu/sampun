import axios from 'axios';
import {RootPath} from './Config';

const Post = (root, data) => {
    const promise = new Promise((resolve, reject) => {
        axios({
            url: `${RootPath}/${root}`,
            data: data,
            method: 'post',
            headers: {
                'Access-Control-Allow-Origin': '*',
             }
        }).then((res) => {
            resolve(res);
        }, (err) => {
            reject(err);
        })

    })


    return promise;
} 

export default Post;

